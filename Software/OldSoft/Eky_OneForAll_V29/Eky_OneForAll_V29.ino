#include <Arduino.h>
#include <PubSubClient.h>
#include <ESP8266WiFi.h>
#include <SPI.h>
#include <W5500lwIP.h>
#include <InfluxDbClient.h>
#include <ESPAsyncTCP.h>        //OTA
#include <ESPAsyncWebServer.h>
#include <AsyncElegantOTA.h>
#include <ArduinoJson.h>
#include <Hash.h>
#include <FS.h>

#define CSPIN 4                       //Pin définit, changement car pin 16 utiliser pour le deepsleep, 
//15 démarre en mode flash, 02 empêche la connection MQTT

char message_buff[100];               //Buffer qui permet de décoder les messages MQTT reçus

long lastMsg = 0;                     //Horodatage du dernier message publié sur MQTT
long lastRecu = 0;
bool debug = false;                   //Affiche sur la console si True
byte mac[] = {0x00, 0xAA, 0xBB, 0xCC, 0xDE, 0x02}; // A modifier pour

Wiznet5500lwIP eth(CSPIN);
AsyncWebServer server(80);

/*Parametres Wifi*/
String wifi_ssid;
String wifi_password;
const char *ssid = "XKY";
const char *password = "oneforall";
WiFiClient espClient; //A corriger ou modifier
int maxtrywificon = 2000;
int nbtrywificon = 0;
String adressmacesp = "";

/*Parametres Linky*/
boolean linky_histstard = 0;
String strModeTestCom;

/*Parametres xKy*/
long MQAll;
long DeepModulateSleeptime;
long Ledtimer;
long Timestart;
boolean Ledstate;
long Timestateled;
long timestartfunc = millis();
long calibtimeledoff = 2899;
long calibtimeledon = 99;
long calibtimefault = 9999;
long rechargetime = 25000;
String FWVersion = "V29";
String BoardVersion = "Eky";

/*Parametres InfluxDB*/
boolean useinflux;
String struseinflux;
String INFLUXDB_URL;
// InfluxDB 2 server or cloud API authentication token (Use: InfluxDB UI -> Load Data -> Tokens -> <select token>)
String INFLUXDB_TOKEN;
// InfluxDB 2 organization id (Use: InfluxDB UI -> Settings -> Profile -> <name under tile> )
String INFLUXDB_ORG;
// InfluxDB 2 bucket name (Use: InfluxDB UI -> Load Data -> Buckets)
String INFLUXDB_BUCKET;

/*Parametres MQTT*/
boolean usemqtt;
String mqtt_server;
String mqtt_user;
String mqtt_password;
PubSubClient client(espClient);
int maxtrymqttcon = 5;
int nbtrymqttcon = 0;
boolean mqttproblem = 0;

// Variables Linky historique
char HHPHC;
int ISOUSC;               // intensité souscrite
int PEJP;
int IINST;                // intensité instantanée    en A
int IINST1;
int IINST2;
int IINST3;
int ADPS;
int PAPP;                 // puissance apparente      en VA
unsigned long PMAX;       // compteur Heures Creuses  en Wh
unsigned long HCHC;       // compteur Heures Creuses  en Wh
unsigned long HCHP;       // compteur Heures Pleines  en Wh
unsigned long EJPHN;       // compteur Heures Normales  en Wh
unsigned long EJPHPM;       // compteur Heures de Pointe Mobile  en Wh
unsigned long BASE;       // index BASE               en Wh
unsigned long BBRHCJB;       // compteur Heures Creuses  en Wh
unsigned long BBRHPJB;       // compteur Heures Pleines  en Wh
unsigned long BBRHCJW;       // compteur Heures Creuses  en Wh
unsigned long BBRHPJW;       // compteur Heures Pleines  en Wh
unsigned long BBRHCJR;       // compteur Heures Creuses  en Wh
unsigned long BBRHPJR;       // compteur Heures Pleines  en Wh
String DEMAIN;              // demain rouge
String PTEC;              // période tarif en cours
String ADCO;              // adresse du compteur
String OPTARIF;           // option tarifaire
int IMAX;                 // intensité maxi = 90A
int IMAX1;
int IMAX2;
int IMAX3;
String MOTDETAT;          // status word

char HHPHCstr;
int ISOUSCstr;               // intensité souscrite
int PEJPstr;
int IINSTstr;                // intensité instantanée    en A
int IINST1str;
int IINST2str;
int IINST3str;
int ADPSstr;
int PAPPstr;                 // puissance apparente      en VA
unsigned long PMAXstr;       // compteur Heures Creuses  en Wh
unsigned long HCHCstr;       // compteur Heures Creuses  en Wh
unsigned long HCHPstr;       // compteur Heures Pleines  en Wh
unsigned long EJPHNstr;       // compteur Heures Normales  en Wh
unsigned long EJPHPMstr;       // compteur Heures de Pointe Mobile  en Wh
unsigned long BASEstr;       // index BASE               en Wh
unsigned long BBRHCJBstr;       // compteur Heures Creuses  en Wh
unsigned long BBRHPJBstr;       // compteur Heures Pleines  en Wh
unsigned long BBRHCJWstr;       // compteur Heures Creuses  en Wh
unsigned long BBRHPJWstr;       // compteur Heures Pleines  en Wh
unsigned long BBRHCJRstr;       // compteur Heures Creuses  en Wh
unsigned long BBRHPJRstr;       // compteur Heures Pleines  en Wh
String DEMAINstr;              // demain rouge
String PTECstr;              // période tarif en cours
String ADCOstr;              // adresse du compteur
String OPTARIFstr;           // option tarifaire
int IMAXstr;                 // intensité maxi = 90A
int IMAX1str;
int IMAX2str;
int IMAX3str;
String MOTDETATstr;          // status word

/*Parametres Validation*/
boolean okHHPHC = 0;
boolean okISOUSC = 0;
boolean okPEJP = 0;
boolean okIINST = 0;
boolean okIINST1 = 0;
boolean okIINST2 = 0;
boolean okIINST3 = 0;
boolean okADPS = 0;
boolean okPAPP = 0;
boolean okPMAX = 0;
boolean okHCHC = 0;
boolean okHCHP = 0;
boolean okEJPHN = 0;
boolean okEJPHPM = 0;
boolean okBASE = 0;
boolean okBBRHCJB = 0;
boolean okBBRHPJB = 0;
boolean okBBRHCJW = 0;
boolean okBBRHPJW = 0;
boolean okBBRHCJR = 0;
boolean okBBRHPJR = 0;
boolean okDEMAIN = 0;
boolean okPTEC = 0;
boolean okADCO = 0;
boolean okOPTARIF = 0;
boolean okIMAX = 0;
boolean okIMAX1 = 0;
boolean okIMAX2 = 0;
boolean okIMAX3 = 0;
boolean okMOTDETAT = 0;

/*Parametres Page WEB*/
const char* PARAM_URL = "inputUrl";
const char* PARAM_TOKEN = "inputToken";
const char* PARAM_ORG = "inputOrg";
const char* PARAM_BUCKET = "inputBucket";
const char* PARAM_WIFI_SSID = "inputWifiSsid";
const char* PARAM_WIFI_PASSWORD = "inputWifiPassword";
const char* PARAM_ACTIVE_INFLUX = "inputActiverInflux";
const char* PARAM_ACTIVE_MQTT = "inputActiverMqtt";
const char* PARAM_URL_MQTT = "inputUrlMqtt";
const char* PARAM_USER_MQTT = "inputUserMqtt";
const char* PARAM_PASSWORD_MQTT = "inputPasswordMqtt";
const char* PARAM_MODEHISTSTAND = "inputModeHistStand";
const char* PARAM_MAINTOPIC_MQTT = "inputMainTopicMqtt";
const char* PARAM_MODETESTCOM = "inputModeTestCom";
const char* PARAM_OPTIMDSSECONDS = "inputOptimiDSSeconds";

String struseoptimwifi;
String struseled;
String strmidtrywificon;
String strDeepSleepSecondsOpti;
String strINFLUXDB_URL;
String strINFLUXDB_TOKEN;
String strINFLUXDB_ORG;
String strINFLUXDB_BUCKET;
String strwifi_ssid;
String strwifi_password;
String strwifi_passwordhtmlclean;
String strusemqtt;
String strmqtt_server;
String strmqtt_user;
String strmqtt_password;
String strmaintopicmqtt;
String strlinky_histstard;
String strscanwifitemp;
String strscanwifi;

unsigned long DeepSleepSecondsOpti = 0;

/*Parameter HTML*/

// HTML web page to handle 3 input fields (inputUrl, inputToken, inputOrg, inputBucket)
const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html><head>
  <title>XKY Configuration</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script>
    function submitMessage() {
      alert("Les valeurs vont etre sauvegardees dans le XKY");
      setTimeout(function(){ document.location.reload(false); }, 5000);   
    }
    function rebootMessage() {
      alert("Le Module va rebooter");
      setTimeout(function(){ document.location.replace("reboot"); }, 1000);   
    }
  </script></head><body>
  <p><b>Configuration du module XKY</b></p>
  <p>Configuration Linky</p>
  <form action="/get" target="hidden-form">
      <label for="Mode">Mode actuel: %inputModeHistStand%</label>
      <select name="inputModeHistStand" id="abon">
        <option value="Historique">Historique</option>
        <option value="Standard">Standard</option>
      </select>
      <p><b>Mode Test communication sans TIC</b></p>
    <label for="test">Mode test actuel: %inputModeTestCom%</label>
      <select name="inputModeTestCom" id="test">
        <option value="Active">Active</option>
        <option value="Desactive">Desactive</option>
        <option selected hidden>%inputModeTestCom%</option>
      </select><br><br>
     Intervalle de remontee (en secondes) valeur actuelle: %inputOptimiDSSeconds% <input type="text" name="inputOptimiDSSeconds"><br><br>
  <input type="submit" value="Enregistrer les modifications" onclick="submitMessage()">
  <input type="submit" value="Reboot" onclick="rebootMessage()">  
  </form><br>
  <a href="influx">Configuration Influxdb</a>
  <a href="mqtt">Configuration MQTT</a>
  <a href="compthist">Lecture Tic Historique</a>
  <a href="update">Update</a>
  <p>One For All version 1er detenteur Board: %BoardVersion% Version soft: %FWVersion% </p>
  <iframe style="display:none" name="hidden-form"></iframe>
</body></html>)rawliteral";

// HTML web page to handle 3 input fields (inputUrl, inputToken, inputOrg, inputBucket)
const char influx_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html><head>
  <title>XKY Configuration</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script>
    function submitMessage() {
      alert("Les valeurs vont etre sauvegardees dans le XKY");
      setTimeout(function(){ document.location.reload(false); }, 5000);   
    }
    function rebootMessage() {
      alert("Le Module va rebooter");
      setTimeout(function(){ document.location.replace("reboot"); }, 1000);   
    }
  </script></head><body>
  <p><b>Configuration du module XKY</b></p>
  <p>Server InfluxDB</p>
  <form action="/get" target="hidden-form">
  <label for="Mode">Activation InfluxDB actuel: %inputActiverInflux%</label>
      <select name="inputActiverInflux" id="Mode">
        <option value="Active">Active</option>
        <option value="Desactive">Desactive</option>
        <option selected hidden>%inputActiverInflux%</option>
      </select><br><br>
    Url actuelle: %inputUrl% <input type="text" name="inputUrl"><br><br>
    Token actuel: %inputToken% <input type="text " name="inputToken"><br><br>
    Organisation actuelle: %inputOrg% <input type="text " name="inputOrg"><br><br>
    Bucket actuel: %inputBucket% <input type="text " name="inputBucket"><br><br>
    <input type="submit" value="Enregistrer les modifications" onclick="submitMessage()">
    <input type="submit" value="Reboot" onclick="rebootMessage()">  
  </form>
  <br>
  <a href="index">Configuration Generale</a>
  <a href="mqtt">Configuration MQTT</a>
  <a href="compthist">Lecture Tic Historique</a>
  <a href="update">Update</a>
  <p>One For All version 1er detenteur Board: %BoardVersion% Version soft: %FWVersion% </p>
  <iframe style="display:none" name="hidden-form"></iframe>
</body></html>)rawliteral";

// HTML web page to handle 3 input fields (inputUrl, inputToken, inputOrg, inputBucket)
const char mqtt_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html><head>
  <title>XKY Configuration</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script>
    function submitMessage() {
      alert("Les valeurs vont etre sauvegardees dans le XKY");
      setTimeout(function(){ document.location.reload(false); }, 5000);   
    }
    function rebootMessage() {
      alert("Le Module va rebooter");
      setTimeout(function(){ document.location.replace("reboot"); }, 1000);   
    }
  </script></head><body>
  <p><b>Configuration du module XKY</b></p>
  <p>Server MQTT</p>
  <form action="/get" target="hidden-form">
  <label for="Mode">Activation MQTT actuel: %inputActiverMqtt%</label>
      <select name="inputActiverMqtt" id="Mode">
        <option value="Active">Active</option>
        <option value="Desactive">Desactive</option>
        <option selected hidden>%inputActiverMqtt%</option>
      </select><br><br>
    Url actuelle: %inputUrlMqtt% <input type="text" name="inputUrlMqtt"><br><br>
    User actuel: %inputUserMqtt% <input type="text " name="inputUserMqtt"><br><br>
    Password actuel: %inputPasswordMqtt% <input type="text " name="inputPasswordMqtt"><br><br>
    Topic actuel: %inputMainTopicMqtt% <input type="text " name="inputMainTopicMqtt"><br><br>
    <input type="submit" value="Enregistrer les modifications" onclick="submitMessage()">
    <input type="submit" value="Reboot" onclick="rebootMessage()">  
  </form>
  <br> 
  <a href="index">Configuration Generale</a>
  <a href="influx">Configuration InfluxDB</a>
  <a href="compthist">Lecture Tic Historique</a>
  <a href="update">Update</a>
  <p>One For All version 1er detenteur Board: %BoardVersion% Version soft: %FWVersion% </p>
  <iframe style="display:none" name="hidden-form"></iframe>
</body></html>)rawliteral";

// HTML web page to handle 3 input fields (inputUrl, inputToken, inputOrg, inputBucket)
const char compthist_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html><head>
  <title>XKY Configuration</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script>
    function submitMessage() {
      alert("Les valeurs vont etre sauvegardees dans le XKY");
      setTimeout(function(){ document.location.reload(false); }, 5000);   
    }
    function rebootMessage() {
      alert("Le Module va rebooter");
      setTimeout(function(){ document.location.replace("reboot"); }, 1000);   
    }
  </script></head><body>
  <p><b>Valeurs Linky actuelles</b></p>
  <p><b>Communes</b></p>
  <form action="/get" target="hidden-form">
      <label for="Mode">ADCO actuel: %ADCO%</label><br>
      <label for="Mode">OPTARIF actuel: %OPTARIF%</label><br>
      <label for="Mode">PAPP actuel: %PAPP%</label><br>
      <label for="Mode">BASE actuel: %BASE%</label><br>
      <label for="Mode">HHPHC actuel: %HHPHC%</label><br>
      <label for="Mode">ISOUSC actuel: %ISOUSC%</label><br>
      <label for="Mode">PTEC actuel: %PTEC%</label><br>
      <label for="Mode">MOTDETAT actuel: %MOTDETAT%</label><br>
  </form>
  <p><b>Valeurs speciales Heures Creuses Heures Pleines</b></p>
      <label for="Mode">HCHC actuel: %HCHC%</label><br>
      <label for="Mode">HCHP actuel: %HCHP%</label><br>
  </form>
  <p><b>Valeurs speciales Tempo</b></p>
      <label for="Mode">BBRHCJB actuel: %BBRHCJB%</label><br>
      <label for="Mode">BBRHPJB actuel: %BBRHPJB%</label><br>
      <label for="Mode">BBRHCJW actuel: %BBRHCJW%</label><br>
      <label for="Mode">BBRHPJW actuel: %BBRHPJW%</label><br>
      <label for="Mode">BBRHCJR actuel: %BBRHCJR%</label><br>
      <label for="Mode">BBRHPJR actuel: %BBRHPJR%</label><br>
      <label for="Mode">DEMAIN actuel: %DEMAIN%</label><br>
  </form>
  <p><b>Valeurs speciales EJP</b></p>
      <label for="Mode">EJPHN actuel: %EJPHN%</label><br>
      <label for="Mode">EJPHPM actuel: %EJPHPM%</label><br>
      <label for="Mode">PEJP actuel: %PEJP%</label><br>
  </form>
  <p><b>Speciales Monophase</b></p>
  <form action="/get" target="hidden-form">
      <label for="Mode">IINST actuel: %IINST%</label><br>
      <label for="Mode">IMAX actuel: %IMAX%</label><br>
      <label for="Mode">ADPS actuel: %ADPS%</label><br>
  </form>
  <p><b>Speciales Triphase</b></p>
  <form action="/get" target="hidden-form">
      <label for="Mode">IINST1 actuel: %IINST1%</label><br>
      <label for="Mode">IINST2 actuel: %IINST2%</label><br>
      <label for="Mode">IINST3 actuel: %IINST3%</label><br>
      <label for="Mode">IMAX1 actuel: %IMAX1%</label><br>
      <label for="Mode">IMAX2 actuel: %IMAX2%</label><br>
      <label for="Mode">IMAX3 actuel: %IMAX3%</label><br>
      <label for="Mode">PMAX actuel: %PMAX%</label><br>
  </form><br>
  <a href="index">Configuration Generale</a>
  <a href="influx">Configuration Influxdb</a>
  <a href="mqtt">Configuration MQTT</a>
  <a href="update">Update</a>
  <p>One For All version 1er detenteur Board: %BoardVersion% Version soft: %FWVersion% </p>
  <iframe style="display:none" name="hidden-form"></iframe>
</body></html>)rawliteral";


const char reboot_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html><head>
  <title>XKY Configuration</title>
  </head></body>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <p><b><u>REBOOT EN COURS</u></b></p>
  <a href="index">Configuration Generale</a>
  <p>One For All version 1er detenteur</p>
  <iframe style="display:none" name="hidden-form"></iframe>
</body></html>)rawliteral";


void notFound(AsyncWebServerRequest *request) {
  request->send(404, "text/plain", "Not found");
}

String readFile(fs::FS &fs, const char * path) {
  Serial.printf("Reading file: %s\r\n", path);
  File file = fs.open(path, "r");
  if (!file || file.isDirectory()) {
    Serial.println("- empty file or failed to open file");
    return String();
  }
  Serial.println("- read from file:");
  String fileContent;
  while (file.available()) {
    fileContent += String((char)file.read());
  }
  file.close();
  Serial.println(fileContent);
  return fileContent;
}

void writeFile(fs::FS &fs, const char * path, const char * message) {
  Serial.printf("Writing file: %s\r\n", path);
  File file = fs.open(path, "w");
  if (!file) {
    Serial.println("- failed to open file for writing");
    return;
  }
  if (file.print(message)) {
    Serial.println("- file written");
  } else {
    Serial.println("- write failed");
  }
  file.close();
}

// Replaces placeholder with stored values
String processor(const String& var) {
  //Serial.println(var);
  if (var == "inputUrl") {
    return INFLUXDB_URL;
  }
  else if (var == "inputToken") {
    return INFLUXDB_TOKEN;
  }
  else if (var == "inputOrg") {
    return INFLUXDB_ORG;
  }
  else if (var == "inputBucket") {
    return INFLUXDB_BUCKET;
  }
  else if (var == "BoardVersion") {
    return BoardVersion;
  }
  else if (var == "FWVersion") {
    return FWVersion;
  }
  else if (var == "inputWifiSsid") {
    return wifi_ssid;
  }
  else if (var == "inputWifiPassword") {
    return wifi_password;
  }
  else if (var == "inputActiverInflux") {
    return struseinflux;
  }
  else if (var == "inputActiverMqtt") {
    return strusemqtt;
  }
  else if (var == "inputUrlMqtt") {
    return mqtt_server;
  }
  else if (var == "inputUserMqtt") {
    return mqtt_user;
  }
  else if (var == "inputPasswordMqtt") {
    return mqtt_password;
  }
  else if (var == "inputModeHistStand") {
    return readFile(SPIFFS, "/inputModeHistStand.txt");
  }
  else if (var == "inputMainTopicMqtt") {
    return strmaintopicmqtt;
  }
  else if (var == "inputModeTestCom") {
    return strModeTestCom;
  }
  else if (var == "inputOptimiDSSeconds") {
    return strDeepSleepSecondsOpti;
  }
  else if (var == "ADCO") {
    return ADCOstr;
  }
  else if (var == "OPTARIF") {
    return OPTARIFstr;
  }
  else if (var == "PEJP") {
    return String(PEJPstr);
  }
  else if (var == "ISOUSC") {
    return String(ISOUSCstr);
  }
  else if (var == "HCHC") {
    return String(HCHCstr);
  }
  else if (var == "HCHP") {
    return String(HCHPstr);
  }
  else if (var == "BASE") {
    return String(BASEstr);
  }
  else if (var == "EJPHN") {
    return String(EJPHNstr);
  }
  else if (var == "EJPHPM") {
    return String(EJPHPMstr);
  }
  else if (var == "BBRHCJB") {
    return String(BBRHCJBstr);
  }
  else if (var == "BBRHPJB") {
    return String(BBRHPJBstr);
  }
  else if (var == "BBRHCJW") {
    return String(BBRHCJWstr);
  }
  else if (var == "BBRHPJW") {
    return String(BBRHPJWstr);
  }
  else if (var == "BBRHCJR") {
    return String(BBRHCJRstr);
  }
  else if (var == "BBRHPJR") {
    return String(BBRHPJRstr);
  }
  else if (var == "BBRHCJR") {
    return String(BBRHCJRstr);
  }
  else if (var == "PTEC") {
    return PTECstr;
  }
  else if (var == "DEMAIN") {
    return DEMAINstr;
  }
  else if (var == "HHPHC") {
    return String(HHPHCstr);
  }
  else if (var == "IINST") {
    return String(IINSTstr);
  }
  else if (var == "IMAX") {
    return String(IMAXstr);
  }
  else if (var == "PAPP") {
    return String(PAPPstr);
  }
  else if (var == "MOTDETAT") {
    return MOTDETATstr;
  }
  else if (var == "IMAX1") {
    return String(IMAX1str);
  }
  else if (var == "IMAX2") {
    return String(IMAX2str);
  }
  else if (var == "IMAX3") {
    return String(IMAX3str);
  }
  else if (var == "IINST1") {
    return String(IINST1str);
  }
  else if (var == "IINST2") {
    return String(IINST2str);
  }
  else if (var == "IINST3") {
    return String(IINST3str);
  }
  else if (var == "ADPS") {
    return  String(ADPSstr);
  }
  else if (var == "PMAX") {
    return String(PMAXstr);
  }
  return String();
}

String macToStr(const uint8_t* mac)
{
  String result;
  for (int i = 0; i < 6; ++i) {
    result += String(mac[i], 16);
    if (i < 5)
      result += ':';
  }
  return result;
}

String composeClientID() {
  //  uint8_t mac[6];
  //WiFi.macAddress(mac);
  String clientId;
  clientId += "eky-";
  clientId += macToStr(mac);
  return clientId;
}

//Reconnexion
void reconnect() {
  //Boucle jusqu'à obtenur une reconnexion
  mqttproblem = 0;
  while (!client.connected()) {
    Serial.print("Connexion au serveur MQTT...");
    String clientnumber = composeClientID();
    client.setBufferSize(2048);
    if (client.connect(clientnumber.c_str(), mqtt_user.c_str(), mqtt_password.c_str())) {
      Serial.println("OK");
    }
    else {
      Serial.print("KO, erreur : ");
      Serial.print(client.state());
      Serial.println(" On attend 1 secondes avant de recommencer");
      delay(1000);
      nbtrymqttcon = nbtrymqttcon + 1;
      if (nbtrymqttcon == maxtrymqttcon) {
        mqttproblem = 1;
        break;
      }
    }
  }
}

#include "linkyhist.h"
#include "linky.h"
#include "test.h"

/* ======================================================================
  Function: Setup
  Purpose : Configuration de la voie, connexion Ethernet ou Wi-Fi et MQTT
  Input   : -
  Output  : -
  Comments: - sans DeepSleep
  ====================================================================== */
void setup() {

  Serial.begin(9600);     //  sortie moniteur
  delay(500);

  if (!SPIFFS.begin()) {
    Serial.println("An Error has occurred while mounting SPIFFS");
    return;
  }
  //  previousstate = readFile(SPIFFS, "/previousstate.txt");
  useinflux = readFile(SPIFFS, "/inputActiverInflux.txt") == "Active";
  struseinflux = readFile(SPIFFS, "/inputActiverInflux.txt");
  strmaintopicmqtt = readFile(SPIFFS, "/inputMainTopicMqtt.txt");
  INFLUXDB_URL = readFile(SPIFFS, "/inputUrl.txt");
  INFLUXDB_TOKEN = readFile(SPIFFS, "/inputToken.txt");
  INFLUXDB_ORG = readFile(SPIFFS, "/inputOrg.txt");
  INFLUXDB_BUCKET = readFile(SPIFFS, "/inputBucket.txt");

  wifi_ssid = readFile(SPIFFS, "/inputWifiSsid.txt");
  wifi_password = readFile(SPIFFS, "/inputWifiPassword.txt");
  usemqtt = readFile(SPIFFS, "/inputActiverMqtt.txt") == "Active";
  strusemqtt = readFile(SPIFFS, "/inputActiverMqtt.txt");
  mqtt_server = readFile(SPIFFS, "/inputUrlMqtt.txt");
  mqtt_user = readFile(SPIFFS, "/inputUserMqtt.txt");
  mqtt_password = readFile(SPIFFS, "/inputPasswordMqtt.txt");
  strModeTestCom = readFile(SPIFFS, "/inputModeTestCom.txt");
  strDeepSleepSecondsOpti = readFile(SPIFFS, "/inputOptimiDSSeconds.txt");
  DeepSleepSecondsOpti = 1000 * ((readFile(SPIFFS, "/inputOptimiDSSeconds.txt")).toFloat());

  linky_histstard = readFile(SPIFFS, "/inputModeHistStand.txt") == "Standard";

  Serial.println(" ");
  Serial.println(F("Boot: version Linky_standart 9600"));
  Serial.println(ESP.getFullVersion());
  delay(1000);

  //-------------- Ethernet --------------
  SPI.begin();
  SPI.setBitOrder(MSBFIRST);
  SPI.setDataMode(SPI_MODE0);
  SPI.setFrequency(4000000);

  delay(1000);
  eth.setDefault(); // use ethernet for default route
  int present = eth.begin(WiFi.macAddress(mac));
  if (!present) {
    Serial.println("no ethernet hardware present");
    while (1);
  }

  Serial.println("connecting ethernet");
  while (!eth.connected()) {
    Serial.print(".");
    delay(1000);
  }
  Serial.println();
  Serial.print("ethernet ip address: ");
  Serial.println(eth.localIP());
  Serial.print("ethernet subnetMask: ");
  Serial.println(eth.subnetMask());
  Serial.print("ethernet gateway: ");
  Serial.println(eth.gatewayIP());

  server.on("/", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send_P(200, "text/html", index_html, processor);
  });

  server.on("/index", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send_P(200, "text/html", index_html, processor);
  });

  server.on("/influx", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send_P(200, "text/html", influx_html, processor);
  });

  server.on("/mqtt", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send_P(200, "text/html", mqtt_html, processor);
  });

  server.on("/compthist", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send_P(200, "text/html", compthist_html, processor);
  });

  server.on("/reboot", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send_P(200, "text/plain", "ok");
    //request->send_P(200, "text/html", index_html, processor);
    delay(500);
    ESP.restart();
  });

  // Send a GET request to <ESP_IP>/get?inputUrl=<inputMessage>
  server.on("/get", HTTP_GET, [] (AsyncWebServerRequest * request) {
    String inputMessage;
    // GET inputUrl value on <ESP_IP>/get?inputUrl=<inputMessage>
    if (request->hasParam(PARAM_URL)) {
      inputMessage = request->getParam(PARAM_URL)->value();
      if (inputMessage != "") {
        writeFile(SPIFFS, "/inputUrl.txt", inputMessage.c_str());
        INFLUXDB_URL = inputMessage.c_str();
      }
    }
    // GET inputToken value on <ESP_IP>/get?inputToken=<inputMessage>
    if (request->hasParam(PARAM_TOKEN)) {
      inputMessage = request->getParam(PARAM_TOKEN)->value();
      if (inputMessage != "") {
        writeFile(SPIFFS, "/inputToken.txt", inputMessage.c_str());
        INFLUXDB_TOKEN = inputMessage.c_str();
      }
    }
    // GET inputOrg value on <ESP_IP>/get?inputOrg=<inputMessage>
    if (request->hasParam(PARAM_ORG)) {
      inputMessage = request->getParam(PARAM_ORG)->value();
      if (inputMessage != "") {
        writeFile(SPIFFS, "/inputOrg.txt", inputMessage.c_str());
        INFLUXDB_ORG = inputMessage.c_str();
      }
    }
    if (request->hasParam(PARAM_BUCKET)) {
      inputMessage = request->getParam(PARAM_BUCKET)->value();
      if (inputMessage != "") {
        writeFile(SPIFFS, "/inputBucket.txt", inputMessage.c_str());
        INFLUXDB_BUCKET = inputMessage.c_str();
      }
    }
    if (request->hasParam(PARAM_WIFI_SSID)) {
      inputMessage = request->getParam(PARAM_WIFI_SSID)->value();
      if (inputMessage != "") {
        writeFile(SPIFFS, "/inputWifiSsid.txt", inputMessage.c_str());
        strwifi_ssid = inputMessage.c_str();
      }
    }
    if (request->hasParam(PARAM_WIFI_PASSWORD)) {
      inputMessage = request->getParam(PARAM_WIFI_PASSWORD)->value();
      if (inputMessage != "") {
        writeFile(SPIFFS, "/inputWifiPassword.txt", inputMessage.c_str());
        strwifi_password = inputMessage.c_str();
        strwifi_passwordhtmlclean = strwifi_password;
        strwifi_passwordhtmlclean.replace("%", "&#37;");
      }
    }
    if (request->hasParam(PARAM_ACTIVE_INFLUX)) {
      inputMessage = request->getParam(PARAM_ACTIVE_INFLUX)->value();
      if (inputMessage != "") {
        writeFile(SPIFFS, "/inputActiverInflux.txt", inputMessage.c_str());
        struseinflux = inputMessage.c_str();
        useinflux = readFile(SPIFFS, "/inputActiverInflux.txt") == "Active";
      }
    }
    if (request->hasParam(PARAM_ACTIVE_MQTT)) {
      inputMessage = request->getParam(PARAM_ACTIVE_MQTT)->value();
      if (inputMessage != "") {
        writeFile(SPIFFS, "/inputActiverMqtt.txt", inputMessage.c_str());
        strusemqtt = inputMessage.c_str();
        usemqtt = readFile(SPIFFS, "/inputActiverMqtt.txt") == "Active";
      }
    }
    if (request->hasParam(PARAM_URL_MQTT)) {
      inputMessage = request->getParam(PARAM_URL_MQTT)->value();
      if (inputMessage != "") {
        writeFile(SPIFFS, "/inputUrlMqtt.txt", inputMessage.c_str());
        strmqtt_server = inputMessage.c_str();
      }
    }
    if (request->hasParam(PARAM_USER_MQTT)) {
      inputMessage = request->getParam(PARAM_USER_MQTT)->value();
      if (inputMessage != "") {
        writeFile(SPIFFS, "/inputUserMqtt.txt", inputMessage.c_str());
        strmqtt_user = inputMessage.c_str();
        mqtt_user = inputMessage.c_str();
      }
    }
    if (request->hasParam(PARAM_PASSWORD_MQTT)) {
      inputMessage = request->getParam(PARAM_PASSWORD_MQTT)->value();
      if (inputMessage != "") {
        writeFile(SPIFFS, "/inputPasswordMqtt.txt", inputMessage.c_str());
        strmqtt_password = inputMessage.c_str();
        mqtt_password = inputMessage.c_str();
      }
    }
    if (request->hasParam(PARAM_MODEHISTSTAND)) {
      inputMessage = request->getParam(PARAM_MODEHISTSTAND)->value();
      writeFile(SPIFFS, "/inputModeHistStand.txt", inputMessage.c_str());
      strlinky_histstard = inputMessage.c_str();
    }
    if (request->hasParam(PARAM_MAINTOPIC_MQTT)) {
      inputMessage = request->getParam(PARAM_MAINTOPIC_MQTT)->value();
      if (inputMessage != "") {
        writeFile(SPIFFS, "/inputMainTopicMqtt.txt", inputMessage.c_str());
        strmaintopicmqtt = inputMessage.c_str();
      }
    }
    if (request->hasParam(PARAM_MODETESTCOM)) {
      inputMessage = request->getParam(PARAM_MODETESTCOM)->value();
      if (inputMessage != "") {
        writeFile(SPIFFS, "/inputModeTestCom.txt", inputMessage.c_str());
        strModeTestCom = inputMessage.c_str();
      }
    }
    if (request->hasParam(PARAM_OPTIMDSSECONDS)) {
      inputMessage = request->getParam(PARAM_OPTIMDSSECONDS)->value();
      if (inputMessage != "") {
        writeFile(SPIFFS, "/inputOptimiDSSeconds.txt", inputMessage.c_str());
        strDeepSleepSecondsOpti = inputMessage.c_str();
        DeepSleepSecondsOpti = 1000 * ((readFile(SPIFFS, "/inputOptimiDSSeconds.txt")).toFloat());
      }
    }
    //   else {
    //     inputMessage = "No message sent";
    //   }
    Serial.println(inputMessage);
    request->send(200, "text/text", inputMessage);
  });
  server.onNotFound(notFound);

  AsyncElegantOTA.begin(&server);    // Start AsyncElegantOTA
  server.begin();
  Serial.println("HTTP server started");



  if (linky_histstard == 1) {
    linky_setup();
  }

  MQAll = millis();
  adressmacesp = composeClientID();
}

//--------------------------------------- LOOP -------------------------------
void loop() {
  if ( strModeTestCom == "Active") {
    linkytest_loop();
  }
  else if (linky_histstard == 0) {
    linky_hist_loop();
  }
  else {
    linky_loop();
  }
}
//----------------------------------------------------------------------------
