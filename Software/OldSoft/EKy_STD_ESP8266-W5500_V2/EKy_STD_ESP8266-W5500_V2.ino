#include <Arduino.h>
#include <PubSubClient.h>
#include <ESP8266WiFi.h>
#include <SPI.h>
#include <W5500lwIP.h>

const char *ssid = "linksys";                       //Nom du réseau Wi-Fi
const char *password = "G2ElabMonitoringHabitat";   //Mdp du réseau Wi-Fi

#define mqtt_server "192.168.1.204"   //Adresse IP où l'interface utilisateur est installée
#define mqtt_user "guest"             //s'il a été configuré sur Mosquitto
#define mqtt_password "guest"         //idem
#define CSPIN 16

#include "linky.h"

char message_buff[100];               //Buffer qui permet de décoder les messages MQTT reçus

long lastMsg = 0;                     //Horodatage du dernier message publié sur MQTT
long lastRecu = 0;
bool debug = false;                   //Affiche sur la console si True
byte mac[] = {0x00, 0xAA, 0xBB, 0xCC, 0xDE, 0x02};

Wiznet5500lwIP eth(CSPIN);


/* ======================================================================
  Function: Setup
  Purpose : Configuration de la voie, connexion Ethernet ou Wi-Fi et MQTT
  Input   : -
  Output  : -
  Comments: - sans DeepSleep
  ====================================================================== */
void setup() { 
  
  Serial.begin(9600);     //  sortie moniteur              
  delay(500);
  Serial.println(" ");
  Serial.println(F("Boot: version Linky_standart 9600"));
  Serial.print(ESP.getFullVersion());
  delay(1000);

//-------------- Ethernet --------------
  SPI.begin();
  SPI.setBitOrder(MSBFIRST);
  SPI.setDataMode(SPI_MODE0);
  SPI.setFrequency(4000000);

  delay(1000);
  eth.setDefault(); // use ethernet for default route
  int present = eth.begin(mac);
  if (!present) {
    Serial.println("no ethernet hardware present");
//    while(1);
  }
  
  Serial.println("connecting ethernet");
  while (!eth.connected()) {
    Serial.print(".");
    delay(1000);
  }
  Serial.println();
  Serial.print("ethernet ip address: ");
  Serial.println(eth.localIP());
  Serial.print("ethernet subnetMask: ");
  Serial.println(eth.subnetMask());
  Serial.print("ethernet gateway: ");
  Serial.println(eth.gatewayIP());

  client.setServer(mqtt_server, 1883); //Configuration de la connexion au serveur MQTT

  delay(1000);
  
  linky_setup();
}

//--------------------------------------- LOOP -------------------------------
void loop() {
  
            linky_loop();
}
//----------------------------------------------------------------------------
