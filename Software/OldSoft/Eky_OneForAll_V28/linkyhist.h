boolean teleInfoReceived;
char msg[50];

char chksum(char *buff, uint8_t len);
boolean handleBuffer(char *bufferTeleinfo, int sequenceNumnber);

// ---------------------------------------------- //
//        Basic constructor for LoKyTIC           //
void TeleInfo() {
  // variables initializations
  ADCO = "000000000000";
  OPTARIF = "----";
  PEJP = 0;
  ISOUSC = 0;
  HCHC = 0L;
  HCHP = 0L;
  BASE = 0L;
  EJPHN = 0L;
  EJPHPM = 0L;
  BBRHCJB = 0L;
  BBRHPJB = 0L;
  BBRHCJW = 0L;
  BBRHPJW = 0L;
  BBRHCJR = 0L;
  BBRHPJR = 0L;
  PTEC = "----";
  DEMAIN = "----";
  HHPHC = '-';
  IINST = 0;
  IMAX = 0;
  PAPP = 0;
  MOTDETAT = "------";
}
// ---------------------------------------------- //

// ---------------------------------------------- //
//           TIC frame capture from Linky         //
boolean readTeleInfo()  {
  //boolean readTeleInfo(bool TIC_state)  {
#define startFrame  0x02
#define endFrame    0x03
#define startLine   0x0A
#define endLine     0x0D
#define maxFrameLen 280

  int comptChar = 0; // variable de comptage des caractères reçus
  char charIn = 0;  // variable de mémorisation du caractère courant en réception
  char bufferTeleinfo[21] = "";
  int bufferLen = 0;
  int checkSum;
  int sequenceNumnber = 0;    // number of information group

  //--- wait for starting frame character
  while (charIn != startFrame)
  { // "Start Text" STX (002 h) is the beginning of the frame
    if (Serial.available())
      charIn = Serial.read() & 0x7F; // Serial.read() vide buffer au fur et à mesure
  } // fin while (tant que) pas caractère 0x02
  //--- wait for the ending frame character
  while (charIn != endFrame)
  { // tant que des octets sont disponibles en lecture : on lit les caractères
    if (Serial.available()) {
      charIn = Serial.read() & 0x7F;
      // incrémente le compteur de caractère reçus
      comptChar++;
      if (charIn == startLine)  bufferLen = 0;
      bufferTeleinfo[bufferLen] = charIn;
      // on utilise une limite max pour éviter String trop long en cas erreur réception
      // ajoute le caractère reçu au String pour les N premiers caractères
      if (charIn == endLine)  {
        checkSum = bufferTeleinfo[bufferLen - 1];
        if (chksum(bufferTeleinfo, bufferLen) == checkSum)  {// we clear the 1st character
          strncpy(&bufferTeleinfo[0], &bufferTeleinfo[1], bufferLen - 3);
          bufferTeleinfo[bufferLen - 3] = 0x00;
          sequenceNumnber++;
          if (! handleBuffer(bufferTeleinfo, sequenceNumnber))  {
            // Serial.println(F("Sequence error ..."));
            return false;
          }
        }
        else  {
          // Serial.println(F("Checksum error!"));
          return false;
        }
      }
      else
        bufferLen++;
    }
    if (comptChar > maxFrameLen)  {
      // Serial.println(F("Overflow error ..."));
      return false;
    }
  }
  return true;
}
// ---------------------------------------------- //

// ---------------------------------------------- //
//   Update new values from TIC for the next TX   //
void updateParameters() {
  Serial.begin(1200);  // Important!!! -> RESTART LoKyTIC
  teleInfoReceived = readTeleInfo();
  Serial.end(); // Important!!! -> STOP LoKyTIC to send packet.
}
// ---------------------------------------------- //

// ---------------------------------------------- //
//               TIC frame parsing                //
boolean handleBuffer(char *bufferTeleinfo, int sequenceNumnber) {
  // create a pointer to the first char after the space
  char* resultString = strchr(bufferTeleinfo, ' ') + 1;
  boolean sequenceIsOK = 0;

  if (sequenceIsOK = (bufferTeleinfo[0] == 'A' && bufferTeleinfo[2] == 'C')) {
    ADCO = String(resultString);
    okADCO = 1;
  }
  else if (sequenceIsOK = (bufferTeleinfo[0] == 'B' && bufferTeleinfo[1] == 'A')) {
    BASE = atol(resultString);
    okBASE = 1;
  }
  else if (sequenceIsOK = (bufferTeleinfo[0] == 'A' && bufferTeleinfo[2] == 'P')) {
    ADPS = atol(resultString);
    okADPS = 1;
  }
  else if (sequenceIsOK = (bufferTeleinfo[0] == 'H' && bufferTeleinfo[3] == 'C')) {
    HCHC = atol(resultString);
    okHCHC = 1;
  }
  else if (sequenceIsOK = (bufferTeleinfo[0] == 'H' && bufferTeleinfo[3] == 'P')) {
    HCHP = atol(resultString);
    okHCHP = 1;
  }
  else if (sequenceIsOK = bufferTeleinfo[1] == 'T') {
    PTEC = String(resultString);
    okPTEC = 1;
  }
  else if (sequenceIsOK = (bufferTeleinfo[0] == 'P' && bufferTeleinfo[1] == 'E')) {
    PEJP = atol(resultString);
    okPEJP = 1;
  }
  else if (sequenceIsOK = bufferTeleinfo[1] == 'M') {
    IMAX = atol(resultString);
    okIMAX = 1;
  }
  else if (sequenceIsOK = (bufferTeleinfo[0] == 'P' && bufferTeleinfo[1] == 'A')) {
    PAPP = atol(resultString);
    okPAPP = 1;
  }
  else if (sequenceIsOK = (bufferTeleinfo[1] == 'I' && bufferTeleinfo[5] == '1')) {
    IINST1 = atol(resultString);
    okIINST1 = 1;
  }
  else if (sequenceIsOK = (bufferTeleinfo[1] == 'I' && bufferTeleinfo[5] == '2')) {
    IINST2 = atol(resultString);
    okIINST2 = 1;
  }
  else if (sequenceIsOK = (bufferTeleinfo[1] == 'I' && bufferTeleinfo[5] == '3')) {
    IINST3 = atol(resultString);
    okIINST3 = 1;
  }
  else if (sequenceIsOK = (bufferTeleinfo[0] == 'I' && bufferTeleinfo[1] == 'I')) {
    IINST = atol(resultString);
    okIINST = 1;
  }
  else if (sequenceIsOK = (bufferTeleinfo[1] == '0' && bufferTeleinfo[4] == '1')) {
    IMAX1 = atol(resultString);
    okIMAX1 = 1;
  }
  else if (sequenceIsOK = (bufferTeleinfo[1] == '0' && bufferTeleinfo[4] == '2')) {
    IMAX2 = atol(resultString);
    okIMAX2 = 1;
  }
  else if (sequenceIsOK = (bufferTeleinfo[1] == '0' && bufferTeleinfo[4] == '3')) {
    IMAX3 = atol(resultString);
    okIMAX3 = 1;
  }
  else if (sequenceIsOK = (bufferTeleinfo[1] == 'M' && bufferTeleinfo[3] == 'X')) {
    PMAX = atol(resultString);
    okPMAX = 1;
  }
  else if (sequenceIsOK = bufferTeleinfo[1] == 'H') {
    HHPHC = resultString[0];
    okHHPHC = 1;
  }
  else if (sequenceIsOK = (bufferTeleinfo[0] == 'E' && bufferTeleinfo[4] == 'N')) {
    EJPHN = atol(resultString);
    okEJPHN = 1;
  }
  else if (sequenceIsOK = (bufferTeleinfo[0] == 'E' && bufferTeleinfo[4] == 'P')) {
    EJPHPM = atol(resultString);
    okEJPHPM = 1;
  }
  else if (sequenceIsOK = (bufferTeleinfo[0] == 'I' && bufferTeleinfo[1] == 'S')) {
    ISOUSC = atol(resultString);
    okISOUSC = 1;
  }
  else if (sequenceIsOK = bufferTeleinfo[0] == 'D') {
    DEMAIN = String(resultString);
    okDEMAIN = 1;
  }
  else if (sequenceIsOK = bufferTeleinfo[0] == 'O') {
    OPTARIF = String(resultString);
    okOPTARIF = 1;
  }
  else if (sequenceIsOK = (bufferTeleinfo[4] == 'C' && bufferTeleinfo[6] == 'B')) {
    BBRHCJB = atol(resultString);
    okBBRHCJB = 1;
  }
  else if (sequenceIsOK = (bufferTeleinfo[4] == 'P' && bufferTeleinfo[6] == 'B')) {
    BBRHPJB = atol(resultString);
    okBBRHPJB = 1;
  }
  else if (sequenceIsOK = (bufferTeleinfo[4] == 'C' && bufferTeleinfo[6] == 'W')) {
    BBRHCJW = atol(resultString);
    okBBRHCJW = 1;
  }
  else if (sequenceIsOK = (bufferTeleinfo[4] == 'P' && bufferTeleinfo[6] == 'W')) {
    BBRHPJW = atol(resultString);
    okBBRHPJW = 1;
  }
  else if (sequenceIsOK = (bufferTeleinfo[4] == 'C' && bufferTeleinfo[6] == 'R')) {
    BBRHCJR = atol(resultString);
    okBBRHCJR = 1;
  }
  else if (sequenceIsOK = (bufferTeleinfo[4] == 'P' && bufferTeleinfo[6] == 'R')) {
    BBRHPJR = atol(resultString);
    okBBRHPJR = 1;
  }
  else if (sequenceIsOK = bufferTeleinfo[1] == 'O') {
    MOTDETAT = String(resultString);
    okMOTDETAT = 1;
  }

  if (!sequenceIsOK) {
    // Serial.print(F("Out of sequence ..."));
    //  Serial.println(bufferTeleinfo);
  }
  return sequenceIsOK;
}
// ---------------------------------------------- //

// ---------------------------------------------- //
//            Checksum Calculation                //
char chksum(char *buff, uint8_t len)  {
  int i;
  char sum = 0;
  for (i = 1; i < (len - 2); i++)
    sum = sum + buff[i];
  sum = (sum & 0x3F) + 0x20;
  return (sum);
}
// ---------------------------------------------- //

void linky_hist_loop() {
  TeleInfo();
  while (!okPAPP || millis() < (MQAll + DeepSleepSecondsOpti) || (!okBASE && (!okHCHP || !okHCHC) && (!okEJPHN || !okEJPHPM) && (!okBBRHCJB || !okBBRHPJB || !okBBRHCJW || !okBBRHPJW || !okBBRHCJR || !okBBRHPJR))) {
    updateParameters();
  }
  if (BASE == 0) {
    BASE = BBRHCJB + BBRHPJB + BBRHCJW + BBRHPJW + BBRHCJR + BBRHPJR + HCHP + HCHC;
  }

  MQAll = millis();
  okPAPP = 0;
  Point sensor("xKy");
  InfluxDBClient clientinflux(INFLUXDB_URL, INFLUXDB_ORG, INFLUXDB_BUCKET, INFLUXDB_TOKEN);
  DynamicJsonDocument JSONbuffer(1024);
  String output;

    sensor.clearFields();

    sensor.addField("ModeTic", "Historique");
    JSONbuffer["ModeTic"] = "Historique";
    sensor.addField("FWVersion", FWVersion);
    JSONbuffer["FWVersion"] = String(FWVersion);
    sensor.addField("BoardVersion", BoardVersion);
    JSONbuffer["BoardVersion"] = String(BoardVersion);
    sensor.addField("ADCO", ADCO);
    JSONbuffer["ADCO"] = String(ADCO);
    sensor.addField("OPTARIF", OPTARIF);
    JSONbuffer["OPTARIF"] = String(OPTARIF);
    sensor.addField("ISOUSC", ISOUSC);
    JSONbuffer["ISOUSC"] = String(ISOUSC);
    sensor.addField("BASE", BASE);
    JSONbuffer["BASE"] = String(BASE);

    if (okHCHP || okHCHC) {
      sensor.addField("HCHC", HCHC);
      sensor.addField("HCHP", HCHP);
      JSONbuffer["HCHC"] = String(HCHC);
      JSONbuffer["HCHP"] = String(HCHP);
    }
    if (okEJPHN || okEJPHPM) {
      sensor.addField("EJPHN", EJPHN);
      sensor.addField("EJPHPM", EJPHPM);
      sensor.addField("PEJP", PEJP);
      JSONbuffer["EJPHN"] = String(EJPHN);
      JSONbuffer["EJPHPM"] = String(EJPHPM);
      JSONbuffer["PEJP"] = String(PEJP);
    }
    if (okBBRHCJB || okBBRHPJB || okBBRHCJW || okBBRHPJW || okBBRHCJR || okBBRHPJR) {
      sensor.addField("BBRHCJB", BBRHCJB);
      sensor.addField("BBRHPJB", BBRHPJB);
      sensor.addField("BBRHCJW", BBRHCJW);
      sensor.addField("BBRHPJW", BBRHPJW);
      sensor.addField("BBRHCJR", BBRHCJR);
      sensor.addField("BBRHPJR", BBRHPJR);
      JSONbuffer["BBRHCJB"] = String(BBRHCJB);
      JSONbuffer["BBRHPJB"] = String(BBRHPJB);
      JSONbuffer["BBRHCJW"] = String(BBRHCJW);
      JSONbuffer["BBRHPJW"] = String(BBRHPJW);
      JSONbuffer["BBRHCJR"] = String(BBRHCJR);
      JSONbuffer["BBRHPJR"] = String(BBRHPJR);
    }
    if (okPMAX) {
      sensor.addField("PMAX", PMAX);
      JSONbuffer["PMAX"] = String(PMAX);
    }
    if (okPEJP) {
      sensor.addField("PEJP", PEJP);
      JSONbuffer["PEJP"] = String(PEJP);
    }
    if (okPTEC) {
      sensor.addField("PTEC", PTEC);
      JSONbuffer["PTEC"] = String(PTEC);
    }
    if (okDEMAIN) {
      sensor.addField("DEMAIN", DEMAIN);
      JSONbuffer["DEMAIN"] = String(DEMAIN);
    }
    if (okIINST) {
      sensor.addField("IINST", IINST);
      JSONbuffer["IINST"] = String(IINST);
    }
    if (okIINST1) {
      sensor.addField("IINST1", IINST1);
      JSONbuffer["IINST1"] = String(IINST1);
    }
    if (okIINST2) {
      sensor.addField("IINST2", IINST2);
      JSONbuffer["IINST2"] = String(IINST2);
    }
    if (okIINST3) {
      sensor.addField("IINST3", IINST3);
      JSONbuffer["IINST3"] = String(IINST3);
    }
    if (okIMAX) {
      sensor.addField("IMAX", IMAX);
      JSONbuffer["IMAX"] = String(IMAX);
    }
    if (okIMAX1) {
      sensor.addField("IMAX1", IMAX1);
      JSONbuffer["IMAX1"] = String(IMAX1);
    }
    if (okIMAX2) {
      sensor.addField("IMAX2", IMAX2);
      JSONbuffer["IMAX2"] = String(IMAX2);
    }
    if (okIMAX3) {
      sensor.addField("IMAX3", IMAX3);
      JSONbuffer["IMAX3"] = String(IMAX3);
    }
    if (okADPS) {
      sensor.addField("ADPS", ADPS);
      JSONbuffer["ADPS"] = String(ADPS);
    }
    sensor.addField("PAPP", PAPP);
    JSONbuffer["PAPP"] = String(PAPP);
    if (okHHPHC) {
      sensor.addField("HHPHC", HHPHC);
      JSONbuffer["HHPHC"] = String(HHPHC);
    }
    if (okMOTDETAT) {
      sensor.addField("MOTDETAT", MOTDETAT);
      JSONbuffer["MOTDETAT"] = String(MOTDETAT);
    }

  if (usemqtt == 1) {
    client.setServer(mqtt_server.c_str(), 1883);    //Configuration de la connexion au serveur MQTT
    if (!client.connected()) {
      reconnect();
    }
    if (mqttproblem == 0) {
      client.loop();
      serializeJson(JSONbuffer, output);
      String maintopic = strmaintopicmqtt + "/" + adressmacesp;
      client.publish(maintopic.c_str(), output.c_str(), true);
    }
  }
  if (useinflux == 1) {
    // Add constant tags - only once
    sensor.addTag("MacAdress", adressmacesp);
    clientinflux.setInsecure();
    // Check server connection
    boolean influxconnec = 0;
    while (!influxconnec) {
      influxconnec = clientinflux.validateConnection();
      delay(100);
    }
    clientinflux.writePoint(sensor);
  }
}
