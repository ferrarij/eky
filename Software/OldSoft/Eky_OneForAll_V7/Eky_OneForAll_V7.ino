#include <Arduino.h>
#include <PubSubClient.h>
#include <ESP8266WiFi.h>
#include <SPI.h>
#include <W5500lwIP.h>
#include <InfluxDbClient.h>
#include <ESPAsyncTCP.h>        //OTA
#include <ESPAsyncWebServer.h>
#include <AsyncElegantOTA.h>
#include <ArduinoJson.h>
#include <Hash.h>
#include <FS.h>

#define CSPIN 4                       //Pin définit, changement car pin 16 utiliser pour le deepsleep, 
                                      //15 démarre en mode flash, 02 empêche la connection MQTT 
                                                                           
char message_buff[100];               //Buffer qui permet de décoder les messages MQTT reçus

long lastMsg = 0;                     //Horodatage du dernier message publié sur MQTT
long lastRecu = 0;
bool debug = false;                   //Affiche sur la console si True
byte mac[] = {0x00, 0xAA, 0xBB, 0xCC, 0xDE, 0x02}; // A modifier pour 

Wiznet5500lwIP eth(CSPIN);
AsyncWebServer server(80);

/*Parametres Wifi*/
String wifi_ssid;
String wifi_password;
const char *ssid = "XKY";
const char *password = "oneforall";
WiFiClient espClient;
int midtrywificon = 150;
int mainconnexiontimeout = 300;
int nbtrywificon = 0;
int channel = 11;
String AdressMacRouteur;
char str[] = "00:00:00:00:00:00";
uint8_t bssid[6];
char* ptr; //start and end pointer for strtol
String adressmacesp="";

/*Parametres Linky*/
boolean progmode =0;
String abonnement; 
int typeabo=0;
boolean linky_histstard =0;
boolean modeprod=0;

/*Parametres xKy*/
long MQAll;
long DeepModulateSleeptime;
boolean useoptimwifi=0;
boolean useled=0;
int DeepSleepSecondsOpti =0;
String FWVersion = "V22";
unsigned long timestartup;
unsigned long timedecode;
unsigned long timeconnexion;


/*Parametres InfluxDB*/
boolean useinflux=0;
String INFLUXDB_URL; 
// InfluxDB 2 server or cloud API authentication token (Use: InfluxDB UI -> Load Data -> Tokens -> <select token>)
String INFLUXDB_TOKEN;
// InfluxDB 2 organization id (Use: InfluxDB UI -> Settings -> Profile -> <name under tile> )
String INFLUXDB_ORG;
// InfluxDB 2 bucket name (Use: InfluxDB UI -> Load Data -> Buckets)
String INFLUXDB_BUCKET;

/*Parametres MQTT*/
boolean usemqtt=0;
String mqtt_server="192.168.1.1"; 
String mqtt_user="guest"; 
String mqtt_password="guest"; 
PubSubClient client(espClient);
int maxtrymqttcon = 5;
int nbtrymqttcon = 0;
boolean mqttproblem =0;

/*Parametres Validation*/
boolean okpapp =0;

/*Parametres Page WEB*/
const char* PARAM_URL = "inputUrl";
const char* PARAM_TOKEN = "inputToken";
const char* PARAM_ORG = "inputOrg";
const char* PARAM_BUCKET = "inputBucket";
const char* PARAM_WIFI_SSID = "inputWifiSsid";
const char* PARAM_WIFI_PASSWORD = "inputWifiPassword";
const char* PARAM_ACTIVE_INFLUX = "inputActiverInflux";
const char* PARAM_ACTIVE_MQTT = "inputActiverMqtt";
const char* PARAM_URL_MQTT = "inputUrlMqtt";
const char* PARAM_USER_MQTT = "inputUserMqtt";
const char* PARAM_PASSWORD_MQTT = "inputPasswordMqtt";
const char* PARAM_ABONNEMENT = "inputAbonnements";
const char* PARAM_MODEPROD = "inputModeProd";
const char* PARAM_MODEHISTSTAND = "inputModeHistStand";
const char* PARAM_ACTIVE_OPTIMWIFI = "inputActiverOptimWifi";
const char* PARAM_MIDCONNEXION = "inputMidConnexion";
const char* PARAM_OPTIMDSSECONDS = "inputOptimiDSSeconds";
const char* PARAM_ACTIVE_LED = "inputActiverLed";
const char* PARAM_MODECONNEXION = "inputModeConnexion";
const char* PARAM_MODETESTCOM = "inputModeTestCom";

String struseoptimwifi;
String struseled;
String strmidtrywificon;
String strDeepSleepSecondsOpti;
String struseinflux;
String strINFLUXDB_URL;
String strINFLUXDB_TOKEN;
String strINFLUXDB_ORG;
String strINFLUXDB_BUCKET;
String strwifi_ssid;
String strwifi_password;
String strusemqtt;
String strmqtt_server;
String strmqtt_user;
String strmqtt_password;
String strabonnement;
String strlinky_histstard;
String strmodeprod;
String strModeConnexion ="Wifi";
String strModeTestCom;

/*Parameter HTML*/

// HTML web page to handle 3 input fields (inputUrl, inputToken, inputOrg, inputBucket)
const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html><head>
  <title>XKY Configuration</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script>
    function submitMessage() {
      alert("Les valeurs vont etre sauvegardees dans le XKY");
      setTimeout(function(){ document.location.reload(false); }, 5000);   
    }
  </script></head><body>
  <p><b><u>Configuration du module XKY</u></b></p>
  <p><b>Wifi</b></p>
  <form action="/get" target="hidden-form">
    SSID actuel: %inputWifiSsid% <input type="text" name="inputWifiSsid"><br><br>
    Mot de passe actuel: %inputWifiPassword% <input type="text" name="inputWifiPassword">
    <p><b>Configuration Connexion</b></p>
    <label for="Conn">Mode choisi actuel: %inputModeConnexion%</label>
      <select name="inputModeConnexion" id="Conn">
        <option value="Wifi">Wifi</option>
        <option value="Ethernet">Ethernet</option>
        <option selected hidden>%inputModeConnexion%</option>
      </select>
    <p><b>Mode Test communication sans TIC</b></p>
    <label for="test">Mode test actuel: %inputModeTestCom%</label>
      <select name="inputModeTestCom" id="test">
        <option value="Active">Active</option>
        <option value="Desactive">Desactive</option>
        <option selected hidden>%inputModeTestCom%</option>
      </select><br><br>
    <p><b>Configuration Linky</b></p>
    <label for="Mode">Mode actuel: %inputModeHistStand%</label>
      <select name="inputModeHistStand" id="Mode">
        <option value="Historique">Historique</option>
        <option value="Standard">Standard</option>
        <option selected hidden>%inputModeHistStand%</option>
      </select>
    <p><b>Configuration Abonnement</b></p>
    <label for="abon">Abonnement actuel: %inputAbonnements%</label>
      <select name="inputAbonnements" id="abon">
        <option value="Base">Base</option>
        <option value="HCHP">HCHP</option>
        <option value="Tempo">Tempo</option>
        <option selected hidden>%inputAbonnements%</option>
      </select>
    <p><b>Mode Production</b></p>
    <label for="prod">Option production actuel: %inputModeProd%</label>
      <select name="inputModeProd" id="prod">
        <option value="Active">Active</option>
        <option value="Desactive">Desactive</option>
        <option selected hidden>%inputModeProd%</option>
      </select><br><br>
    <input type="submit" value="Enregister les modifications" onclick="submitMessage()">  
  </form><br>
  <a href="influx">Configuration Influxdb</a>
  <a href="mqtt">Configuration MQTT</a>
  <a href="avance">Configuration Avancee</a>
  <a href="update">Update</a>
  <p>One For All version 1er detenteur</p>
  <iframe style="display:none" name="hidden-form"></iframe>
</body></html>)rawliteral";

// HTML web page to handle 3 input fields (inputUrl, inputToken, inputOrg, inputBucket)
const char influx_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html><head>
  <title>XKY Configuration</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script>
    function submitMessage() {
      alert("Les valeurs vont etre sauvegardees dans le XKY");
      setTimeout(function(){ document.location.reload(false); }, 5000);   
    }
  </script></head><body>
  <p><b><u>Configuration du module XKY</u></b></p>
  <p><b>Server InfluxDB</b></p>
  <form action="/get" target="hidden-form">
  <label for="Mode">Activation InfluxDB actuel: %inputActiverInflux%</label>
      <select name="inputActiverInflux" id="Mode">
        <option value="Active">Active</option>
        <option value="Desactive">Desactive</option>
        <option selected hidden>%inputActiverInflux%</option>
      </select><br><br>
    Url actuelle: %inputUrl% <input type="text" name="inputUrl"><br><br>
    Token actuel: %inputToken% <input type="text " name="inputToken"><br><br>
    Organisation actuelle: %inputOrg% <input type="text " name="inputOrg"><br><br>
    Bucket actuel: %inputBucket% <input type="text " name="inputBucket"><br><br>
    <input type="submit" value="Enregister les modifications" onclick="submitMessage()">
  </form>
  <br>
  <a href="index">Configuration Generale</a>
  <a href="mqtt">Configuration MQTT</a>
  <a href="avance">Configuration Avancee</a>
  <a href="update">Update</a>
  <p>One For All version 1er detenteur</p>
  <iframe style="display:none" name="hidden-form"></iframe>
</body></html>)rawliteral";

// HTML web page to handle 3 input fields (inputUrl, inputToken, inputOrg, inputBucket)
const char mqtt_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html><head>
  <title>XKY Configuration</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script>
    function submitMessage() {
      alert("Les valeurs vont etre sauvegardees dans le XKY");
      setTimeout(function(){ document.location.reload(false); }, 5000);   
    }
  </script></head><body>
  <p><b><u>Configuration du module XKY</u></b></p>
  <p><b>Server MQTT</b></p>
  <form action="/get" target="hidden-form">
  <label for="Mode">Activation MQTT actuel: %inputActiverMqtt%</label>
      <select name="inputActiverMqtt" id="Mode">
        <option value="Active">Active</option>
        <option value="Desactive">Desactive</option>
        <option selected hidden>%inputActiverMqtt%</option>
      </select><br><br>
    Url actuelle: %inputUrlMqtt% <input type="text" name="inputUrlMqtt"><br><br>
    User actuel: %inputUserMqtt% <input type="text " name="inputUserMqtt"><br><br>
    Password actuel: %inputPasswordMqtt% <input type="text " name="inputPasswordMqtt"><br><br>
    <input type="submit" value="Enregister les modifications" onclick="submitMessage()">
  </form>
  <br>  
  <a href="index">Configuration Generale</a>
  <a href="influx">Configuration InfluxDB</a>
  <a href="avance">Configuration Avancee</a>
  <a href="update">Update</a>
  <p>One For All version 1er detenteur</p>
  <iframe style="display:none" name="hidden-form"></iframe>
</body></html>)rawliteral";

// HTML web page to handle 3 input fields (inputUrl, inputToken, inputOrg, inputBucket)
const char avance_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html><head>
  <title>XKY Configuration</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script>
    function submitMessage() {
      alert("Les valeurs vont etre sauvegardees dans le XKY");
      setTimeout(function(){ document.location.reload(false); }, 5000);   
    }
  </script></head><body>
  <p><b><u>Configuration du module XKY</u></b></p>
  <p><b>Parametres internes XKY</b></p>
  <p>Attention. Ces parametres sont disponible a titre experimental pour vous permettre de modifier le comportement du XKY.</p>
  <p>Un mauvais reglage peut engendrer des instabilites.</p>
  <p>En cas de doute, vous pouvez mettre les activations sur Desactive.</p>
  <form action="/get" target="hidden-form">
  <label for="Mode">Option optimisation wifi actuel: %inputActiverOptimWifi%</label>
      <select name="inputActiverOptimWifi" id="Mode">
        <option value="Active">Active</option>
        <option value="Desactive">Desactive</option>
        <option selected hidden>%inputActiverOptimWifi%</option>
      </select><br><br>
    Nb de secondes validation optimisation wifi (par defaut 15s) valeur actuelle: %inputMidConnexion% <input type="text" name="inputMidConnexion"><br><br>
    Nb de secondes deepsleep optimisation offset (Avec signe + ou - et ici la decimale s'ecrit avec un point exemple 1.5s) (par defaut 0s) valeur actuelle: %inputOptimiDSSeconds% <input type="text" name="inputOptimiDSSeconds"><br><br>
    <label for="led">Option activation LED actuel: %inputActiverLed%</label>
      <select name="inputActiverLed" id="led">
        <option value="Active">Active</option>
        <option value="Desactive">Desactive</option>
        <option selected hidden>%inputActiverLed%</option>
      </select><br><br>
    <input type="submit" value="Enregister les modifications" onclick="submitMessage()">
  </form><br>
  <br>
  <a href="index">Configuration Generale</a>
  <a href="influx">Configuration InfluxDB</a>
  <a href="mqtt">Configuration MQTT</a>
  <a href="update">Update</a>
  <p>One For All version 1er detenteur</p>
  <iframe style="display:none" name="hidden-form"></iframe>
</body></html>)rawliteral";

const char reboot_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html><head>
  <title>XKY Configuration</title>
  </head></body>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <p><b><u>REBOOT EN COURS</u></b></p>
  <a href="index">Configuration Generale</a>
  <p>One For All version 1er detenteur</p>
  <iframe style="display:none" name="hidden-form"></iframe>
</body></html>)rawliteral";


void notFound(AsyncWebServerRequest *request) {
  request->send(404, "text/plain", "Not found");
}

String readFile(fs::FS &fs, const char * path){
  Serial.printf("Reading file: %s\r\n", path);
  File file = fs.open(path, "r");
  if(!file || file.isDirectory()){
    Serial.println("- empty file or failed to open file");
    return String();
  }
  Serial.println("- read from file:");
  String fileContent;
  while(file.available()){
    fileContent+=String((char)file.read());
  }
  file.close();
  Serial.println(fileContent);
  return fileContent;
}

void writeFile(fs::FS &fs, const char * path, const char * message){
  Serial.printf("Writing file: %s\r\n", path);
  File file = fs.open(path, "w");
  if(!file){
    Serial.println("- failed to open file for writing");
    return;
  }
  if(file.print(message)){
    Serial.println("- file written");
  } else {
    Serial.println("- write failed");
  }
  file.close();
}

// Replaces placeholder with stored values
String processor(const String& var){
  //Serial.println(var);
  if(var == "inputUrl"){
    return strINFLUXDB_URL;
  }
  else if(var == "inputToken"){
    return strINFLUXDB_TOKEN;
  }
  else if(var == "inputOrg"){
    return strINFLUXDB_ORG;
  }
  else if(var == "inputBucket"){
    return strINFLUXDB_BUCKET;
  }
  else if(var == "inputWifiSsid"){
    return strwifi_ssid;
  }
  else if(var == "inputWifiPassword"){
    return strwifi_password;
  }
  else if(var == "inputActiverInflux"){
    return struseinflux;
  }
  else if(var == "inputActiverMqtt"){
    return strusemqtt;
  }
  else if(var == "inputUrlMqtt"){
    return strmqtt_server;
  }
  else if(var == "inputUserMqtt"){
    return strmqtt_user;
  }
  else if(var == "inputPasswordMqtt"){
    return strmqtt_password;
  }
  else if(var == "inputAbonnements"){
    return strabonnement;
  }
  else if(var == "inputModeProd"){
    return strmodeprod;
  }
  else if(var == "inputModeHistStand"){
    return strlinky_histstard;
  }
  else if(var == "inputActiverOptimWifi"){
    return struseoptimwifi;
  }
  else if(var == "inputMidConnexion"){
    return strmidtrywificon;
  }
  else if(var == "inputActiverLed"){
    return struseled;
  }
  else if(var == "inputOptimiDSSeconds"){
    return strDeepSleepSecondsOpti;
  }  
  else if(var == "inputModeConnexion"){
    return strModeConnexion;
  }
  else if(var == "inputModeTestCom"){
    return strModeTestCom;
  }
  return String();
}

String macToStr(const uint8_t* mac)
{
  String result;
  for (int i = 0; i < 6; ++i) {
    result += String(mac[i], 16);
    if (i < 5)
      result += ':';
  }
  return result;
}

String composeClientID() {
  uint8_t mac[6];
  //WiFi.macAddress(mac);
  String clientId;
  clientId += "esp-";
  clientId += "00:AA:BB:CC:DE:02";
  return clientId;
}

//Connexion au reseau WiFi
void setup_wifi() {
  delay(10);
  WiFi.setOutputPower(20.5);
  nbtrywificon = 0;
  if(AdressMacRouteur!="" && useoptimwifi){
    WiFi.begin(wifi_ssid, wifi_password,channel,bssid);
    while (WiFi.status() != WL_CONNECTED) {
    delay(100);
    Serial.print(".");
    nbtrywificon = nbtrywificon+1;
    if(nbtrywificon == midtrywificon){
        SPIFFS.remove("/inputMacadress.txt");
    }
    if(nbtrywificon == mainconnexiontimeout){
      WiFi.softAP(ssid, password);
      IPAddress myIP = WiFi.softAPIP();
      break;
    }
  }
  }
  else{
    WiFi.begin(wifi_ssid, wifi_password);
    while (WiFi.status() != WL_CONNECTED) {
    delay(100);
    Serial.print(".");
    nbtrywificon = nbtrywificon+1;
    if(nbtrywificon == mainconnexiontimeout){
      WiFi.softAP(ssid, password);
      IPAddress myIP = WiFi.softAPIP();
      break;
    }
  }
  }
  Serial.println("");
  Serial.println("Connexion WiFi etablie ");
  Serial.print("=> Addresse IP : ");
  Serial.print(WiFi.localIP());
  Serial.printf("\nWiFi channel: %d\n", WiFi.channel());
  Serial.printf("WiFi BSSID: %s\n", WiFi.BSSIDstr().c_str());
  if (AdressMacRouteur != WiFi.BSSIDstr().c_str()){
    writeFile(SPIFFS, "/inputMacadress.txt", WiFi.BSSIDstr().c_str());
  }
  if (String(channel) != String(WiFi.channel())){
    writeFile(SPIFFS, "/inputChannel.txt", String(WiFi.channel()).c_str());
  }
}

//Reconnexion
void reconnect() {
  //Boucle jusqu'à obtenur une reconnexion
  mqttproblem = 0;
  while (!client.connected()) {
    Serial.print("Connexion au serveur MQTT...");
    String clientnumber = composeClientID();
    if (client.connect(clientnumber.c_str(), mqtt_user.c_str(), mqtt_password.c_str())) {
      Serial.println("OK");
    } 
    else {
      Serial.print("KO, erreur : "); 
      Serial.print(client.state());
      Serial.println(" On attend 1 secondes avant de recommencer");
      delay(1000);
      nbtrymqttcon = nbtrymqttcon +1;
      if (nbtrymqttcon == maxtrymqttcon) {
        mqttproblem = 1;
        break;
      }
    }
  }
}

#include "linkyhist.h"
#include "linky.h"

/* ======================================================================
  Function: Setup
  Purpose : Configuration de la voie, connexion Ethernet ou Wi-Fi et MQTT
  Input   : -
  Output  : -
  Comments: - sans DeepSleep
  ====================================================================== */
void setup() { 
  
  Serial.begin(9600);     //  sortie moniteur              
  delay(500);
  Serial.println(" ");
  Serial.println(F("Boot: version Linky_standart 9600"));
  Serial.println(ESP.getFullVersion());
  delay(1000);

   if(!SPIFFS.begin()){
      Serial.println("An Error has occurred while mounting SPIFFS");
      return;
    }
//  previousstate = readFile(SPIFFS, "/previousstate.txt");
  useinflux= readFile(SPIFFS, "/inputActiverInflux.txt") == "Active";
  INFLUXDB_URL = readFile(SPIFFS, "/inputUrl.txt");
  INFLUXDB_TOKEN = readFile(SPIFFS, "/inputToken.txt");
  INFLUXDB_ORG = readFile(SPIFFS, "/inputOrg.txt");
  INFLUXDB_BUCKET = readFile(SPIFFS, "/inputBucket.txt");

  wifi_ssid = readFile(SPIFFS, "/inputWifiSsid.txt");
  wifi_password = readFile(SPIFFS, "/inputWifiPassword.txt");
  usemqtt= readFile(SPIFFS, "/inputActiverMqtt.txt") == "Active";
  mqtt_server= readFile(SPIFFS, "/inputUrlMqtt.txt");
  mqtt_user= readFile(SPIFFS, "/inputUserMqtt.txt");
  mqtt_password= readFile(SPIFFS, "/inputPasswordMqtt.txt");
  abonnement = readFile(SPIFFS, "/inputAbonnements.txt");
  strModeConnexion = readFile(SPIFFS, "/inputModeConnexion.txt");
  strModeTestCom = readFile(SPIFFS, "/inputModeTestCom.txt");
  struseoptimwifi= readFile(SPIFFS, "/inputActiverOptimWifi.txt");
  struseled= readFile(SPIFFS, "/inputActiverLed.txt");
  strmidtrywificon = readFile(SPIFFS, "/inputMidConnexion.txt");
  strDeepSleepSecondsOpti = readFile(SPIFFS, "/inputOptimiDSSeconds.txt");
  
  struseinflux= readFile(SPIFFS, "/inputActiverInflux.txt");
  strINFLUXDB_URL = readFile(SPIFFS, "/inputUrl.txt");
  strINFLUXDB_TOKEN = readFile(SPIFFS, "/inputToken.txt");
  strINFLUXDB_ORG = readFile(SPIFFS, "/inputOrg.txt");
  strINFLUXDB_BUCKET = readFile(SPIFFS, "/inputBucket.txt");

  strwifi_ssid = readFile(SPIFFS, "/inputWifiSsid.txt");
  strwifi_password = readFile(SPIFFS, "/inputWifiPassword.txt");
  strusemqtt= readFile(SPIFFS, "/inputActiverMqtt.txt");
  strmqtt_server= readFile(SPIFFS, "/inputUrlMqtt.txt");
  strmqtt_user= readFile(SPIFFS, "/inputUserMqtt.txt");
  strmqtt_password= readFile(SPIFFS, "/inputPasswordMqtt.txt");
  strabonnement = readFile(SPIFFS, "/inputAbonnements.txt");
  strlinky_histstard = readFile(SPIFFS, "/inputModeHistStand.txt");
  strmodeprod = readFile(SPIFFS, "/inputModeProd.txt");
    
  if(abonnement == "Base" || abonnement == "HCHP"){
    typeabo =0;
  }
  else if(abonnement == "Tempo"){
    typeabo =1;
  }
  linky_histstard = readFile(SPIFFS, "/inputModeHistStand.txt") == "Standard";
  modeprod = readFile(SPIFFS, "/inputModeProd.txt") == "Active";

Serial.println(strModeConnexion);

 if (strModeConnexion=="Ethernet"){
//-------------- Ethernet --------------
  SPI.begin();
  SPI.setBitOrder(MSBFIRST);
  SPI.setDataMode(SPI_MODE0);
  SPI.setFrequency(4000000);

  delay(1000);
  eth.setDefault(); // use ethernet for default route
  int present = eth.begin(mac);
  if (!present) {
    Serial.println("no ethernet hardware present");
    while(1);
  }
  
  Serial.println("connecting ethernet");
  while (!eth.connected()) {
    Serial.print(".");
    delay(1000);
  }
  Serial.println();
  Serial.print("ethernet ip address: ");
  Serial.println(eth.localIP());
  Serial.print("ethernet subnetMask: ");
  Serial.println(eth.subnetMask());
  Serial.print("ethernet gateway: ");
  Serial.println(eth.gatewayIP());
 }
else{
  ///Mettre le processus pour démarrer le wifi
  /// Autrement si pas de wifi on bascule en hotpoint
  Serial.println("toto");
  WiFi.mode(WIFI_STA);
  delay(1);
  setup_wifi();           //On se connecte au réseau wifi
  delay(500);
}
 
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/html", index_html, processor);
  });

  server.on("/index", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/html", index_html, processor);
  });

  server.on("/influx", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/html", influx_html, processor);
  });

  server.on("/mqtt", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/html", mqtt_html, processor);
  });

  server.on("/avance", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/html", avance_html, processor);
  });

  // handler for the "Reboot" button:
  server.on("/reboot", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/plain", "ok");
    //request->send_P(200, "text/html", index_html, processor);
        delay(500);
        ESP.restart();
    });

  // Send a GET request to <ESP_IP>/get?inputUrl=<inputMessage>
  server.on("/get", HTTP_GET, [] (AsyncWebServerRequest *request) {
    String inputMessage;
    // GET inputUrl value on <ESP_IP>/get?inputUrl=<inputMessage>
    if (request->hasParam(PARAM_URL)) {
      inputMessage = request->getParam(PARAM_URL)->value();
      if (inputMessage !=""){
      writeFile(SPIFFS, "/inputUrl.txt", inputMessage.c_str());
      strINFLUXDB_URL = inputMessage.c_str();
      }
    }
    // GET inputToken value on <ESP_IP>/get?inputToken=<inputMessage>
    if (request->hasParam(PARAM_TOKEN)) {
      inputMessage = request->getParam(PARAM_TOKEN)->value();
      if (inputMessage !=""){
      writeFile(SPIFFS, "/inputToken.txt", inputMessage.c_str());
      strINFLUXDB_TOKEN = inputMessage.c_str();
      }
    }
    // GET inputOrg value on <ESP_IP>/get?inputOrg=<inputMessage>
    if (request->hasParam(PARAM_ORG)) {
      inputMessage = request->getParam(PARAM_ORG)->value();
      if (inputMessage !=""){
      writeFile(SPIFFS, "/inputOrg.txt", inputMessage.c_str());
      strINFLUXDB_ORG = inputMessage.c_str();
      }
    }
    if (request->hasParam(PARAM_BUCKET)) {
      inputMessage = request->getParam(PARAM_BUCKET)->value();
      if (inputMessage !=""){
      writeFile(SPIFFS, "/inputBucket.txt", inputMessage.c_str());
      strINFLUXDB_BUCKET = inputMessage.c_str();
      }
    }
    if (request->hasParam(PARAM_WIFI_SSID)) {
      inputMessage = request->getParam(PARAM_WIFI_SSID)->value();
      if (inputMessage !=""){
        writeFile(SPIFFS, "/inputWifiSsid.txt", inputMessage.c_str());
        strwifi_ssid = inputMessage.c_str();
      }
    }
    if (request->hasParam(PARAM_WIFI_PASSWORD)) {
      inputMessage = request->getParam(PARAM_WIFI_PASSWORD)->value();
      if (inputMessage !=""){
      writeFile(SPIFFS, "/inputWifiPassword.txt", inputMessage.c_str());
      strwifi_password = inputMessage.c_str();
      }
    }  
    if (request->hasParam(PARAM_ACTIVE_INFLUX)) {
      inputMessage = request->getParam(PARAM_ACTIVE_INFLUX)->value();
      if (inputMessage !=""){
      writeFile(SPIFFS, "/inputActiverInflux.txt", inputMessage.c_str());
      struseinflux = inputMessage.c_str();
      }
    } 
    if (request->hasParam(PARAM_ACTIVE_MQTT)) {
      inputMessage = request->getParam(PARAM_ACTIVE_MQTT)->value();
      if (inputMessage !=""){
      writeFile(SPIFFS, "/inputActiverMqtt.txt", inputMessage.c_str());
      strusemqtt = inputMessage.c_str();
      }
    } 
    if (request->hasParam(PARAM_URL_MQTT)) {
      inputMessage = request->getParam(PARAM_URL_MQTT)->value();
      if (inputMessage !=""){
      writeFile(SPIFFS, "/inputUrlMqtt.txt", inputMessage.c_str());
      strmqtt_server = inputMessage.c_str();
      }
    } 
    if (request->hasParam(PARAM_USER_MQTT)) {
      inputMessage = request->getParam(PARAM_USER_MQTT)->value();
      if (inputMessage !=""){
      writeFile(SPIFFS, "/inputUserMqtt.txt", inputMessage.c_str());
      strmqtt_user = inputMessage.c_str();
      }
    } 
    if (request->hasParam(PARAM_PASSWORD_MQTT)) {
      inputMessage = request->getParam(PARAM_PASSWORD_MQTT)->value();
      if (inputMessage !=""){
      writeFile(SPIFFS, "/inputPasswordMqtt.txt", inputMessage.c_str());
      strmqtt_password = inputMessage.c_str();
      }
    } 
    if (request->hasParam(PARAM_ABONNEMENT)) {
      inputMessage = request->getParam(PARAM_ABONNEMENT)->value();
      writeFile(SPIFFS, "/inputAbonnements.txt", inputMessage.c_str());
      strabonnement = inputMessage.c_str();
    } 
    if (request->hasParam(PARAM_MODEPROD)) {
      inputMessage = request->getParam(PARAM_MODEPROD)->value();
      if (inputMessage !=""){
      writeFile(SPIFFS, "/inputModeProd.txt", inputMessage.c_str());
      strmodeprod = inputMessage.c_str();
      }
    } 
    if (request->hasParam(PARAM_MODEHISTSTAND)) {
      inputMessage = request->getParam(PARAM_MODEHISTSTAND)->value();
      writeFile(SPIFFS, "/inputModeHistStand.txt", inputMessage.c_str());
      strlinky_histstard = inputMessage.c_str();
    } 
    if (request->hasParam(PARAM_ACTIVE_OPTIMWIFI)) {
      inputMessage = request->getParam(PARAM_ACTIVE_OPTIMWIFI)->value();
      if (inputMessage !=""){
      writeFile(SPIFFS, "/inputActiverOptimWifi.txt", inputMessage.c_str());
      struseoptimwifi = inputMessage.c_str();
      }
    } 
    if (request->hasParam(PARAM_MIDCONNEXION)) {
      inputMessage = request->getParam(PARAM_MIDCONNEXION)->value();
      if (inputMessage !=""){
      writeFile(SPIFFS, "/inputMidConnexion.txt", inputMessage.c_str());
      strmidtrywificon = inputMessage.c_str();
      }
    } 
    if (request->hasParam(PARAM_ACTIVE_LED)) {
      inputMessage = request->getParam(PARAM_ACTIVE_LED)->value();
      if (inputMessage !=""){
      writeFile(SPIFFS, "/inputActiverLed.txt", inputMessage.c_str());
      struseled = inputMessage.c_str();
      }
    } 
    if (request->hasParam(PARAM_OPTIMDSSECONDS)) {
      inputMessage = request->getParam(PARAM_OPTIMDSSECONDS)->value();
      if (inputMessage !=""){
      writeFile(SPIFFS, "/inputOptimiDSSeconds.txt", inputMessage.c_str());
      strDeepSleepSecondsOpti = inputMessage.c_str();
      }
    }  
    if (request->hasParam(PARAM_MODECONNEXION)) {
      inputMessage = request->getParam(PARAM_MODECONNEXION)->value();
      if (inputMessage !=""){
      writeFile(SPIFFS, "/inputModeConnexion.txt", inputMessage.c_str());
      strModeConnexion = inputMessage.c_str();
      }
    } 
    if (request->hasParam(PARAM_MODETESTCOM)) {
      inputMessage = request->getParam(PARAM_MODETESTCOM)->value();
      if (inputMessage !=""){
      writeFile(SPIFFS, "/inputModeTestCom.txt", inputMessage.c_str());
      strModeTestCom = inputMessage.c_str();
      }
    } 
//    else {
//      inputMessage = "No message sent";
//    }
    Serial.println(inputMessage);
    request->send(200, "text/text", inputMessage);
  });
  server.onNotFound(notFound);
  
  AsyncElegantOTA.begin(&server);    // Start AsyncElegantOTA
  server.begin();
  Serial.println("HTTP server started");
  
  if(linky_histstard==1){
    linky_setup();
    }
    
  MQAll = millis();
  adressmacesp = composeClientID();
}

//--------------------------------------- LOOP -------------------------------
void loop() {
  if(linky_histstard==0){
    linky_hist_loop();
  }
  else{
    linky_loop();
    }
}
//----------------------------------------------------------------------------
