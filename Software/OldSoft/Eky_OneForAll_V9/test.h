int testnumber = 0;
void linkytest_loop() {

  while (millis() < (MQAll + 10000)){
   Serial.print("attente ");
   delay(100);
  }
  MQAll = millis();

  Point sensor("xKy");
  InfluxDBClient clientinflux(INFLUXDB_URL, INFLUXDB_ORG, INFLUXDB_BUCKET, INFLUXDB_TOKEN);
  DynamicJsonDocument JSONbuffer(1024);
  String output;
  testnumber = testnumber +1;
  if(useinflux == 0 && usemqtt == 0){
    delay(30000);
  }

  if (useinflux == 1) {
    sensor.clearFields();
    
    sensor.addField("TEST", testnumber);
  }
  if (usemqtt == 1) {
    JSONbuffer["TEST"] = String(testnumber);
  }

  if (useinflux == 1) {
    // Add constant tags - only once
    sensor.addTag("MacAdress", adressmacesp);
    clientinflux.setInsecure();
    // Check server connection
    if (clientinflux.validateConnection()) {
      Serial.print("Connected to InfluxDB: ");
      Serial.println(clientinflux.getServerUrl());
    } else {
      Serial.print("InfluxDB connection failed: ");
      Serial.println(clientinflux.getLastErrorMessage());
    }
   // sensor.addField("RSSI", WiFi.RSSI());
    clientinflux.writePoint(sensor);
  }
  if (usemqtt == 1) {
    client.setServer(mqtt_server.c_str(), 1883);    //Configuration de la connexion au serveur MQTT
    if (!client.connected()) {
      reconnect();
    }
    if (mqttproblem == 0) {
      client.loop();
      serializeJson(JSONbuffer, output);
      String maintopic = "winky/" + adressmacesp;
      client.publish(maintopic.c_str(), output.c_str(), true);
    }
  }
}
