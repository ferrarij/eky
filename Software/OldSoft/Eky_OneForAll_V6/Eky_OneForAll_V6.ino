#include <Arduino.h>
#include <PubSubClient.h>
#include <ESP8266WiFi.h>
#include <SPI.h>
#include <W5500lwIP.h>
#include <InfluxDbClient.h>
#include <ESPAsyncTCP.h>        //OTA
#include <ESPAsyncWebServer.h>
#include <AsyncElegantOTA.h>
#include <ArduinoJson.h>
#include <Hash.h>
#include <FS.h>

#define CSPIN 4                       //Pin définit, changement car pin 16 utiliser pour le deepsleep, 
                                      //15 démarre en mode flash, 02 empêche la connection MQTT 
                                      
char message_buff[100];               //Buffer qui permet de décoder les messages MQTT reçus

long lastMsg = 0;                     //Horodatage du dernier message publié sur MQTT
long lastRecu = 0;
bool debug = false;                   //Affiche sur la console si True
byte mac[] = {0x00, 0xAA, 0xBB, 0xCC, 0xDE, 0x04}; // A modifier pour 

Wiznet5500lwIP eth(CSPIN);
AsyncWebServer server(80);

/*Parametres Wifi*/
String wifi_ssid;
String wifi_password;
const char *ssid = "XKY";
const char *password = "oneforall";
WiFiClient espClient; //A corriger ou modifier
int maxtrywificon = 2000; 
int nbtrywificon = 0;
String adressmacesp="";

/*Parametres Linky*/
boolean progmode =0;
String abonnement; 
int typeabo=0;
boolean linky_histstard =0;
boolean modeprod=0;

/*Parametres xKy*/
long MQAll;
long DeepModulateSleeptime;
long Ledtimer;
long Timestart;
boolean Ledstate;
long Timestateled;
long timestartfunc = millis();
long calibtimeledoff = 2899;
long calibtimeledon = 99;
long calibtimefault = 9999;
long rechargetime = 25000;

/*Parametres InfluxDB*/
boolean useinflux=0;
String INFLUXDB_URL; 
// InfluxDB 2 server or cloud API authentication token (Use: InfluxDB UI -> Load Data -> Tokens -> <select token>)
String INFLUXDB_TOKEN;
// InfluxDB 2 organization id (Use: InfluxDB UI -> Settings -> Profile -> <name under tile> )
String INFLUXDB_ORG;
// InfluxDB 2 bucket name (Use: InfluxDB UI -> Load Data -> Buckets)
String INFLUXDB_BUCKET;

/*Parametres MQTT*/
boolean usemqtt=0;
String mqtt_server="192.168.1.1"; 
String mqtt_user="guest"; 
String mqtt_password="guest"; 
PubSubClient client(espClient);
int maxtrymqttcon = 5;
int nbtrymqttcon = 0;
boolean mqttproblem =0;

/*Parametres Validation*/
boolean okpapp =0;

/*Parametres Page WEB*/
const char* PARAM_URL = "inputUrl";
const char* PARAM_TOKEN = "inputToken";
const char* PARAM_ORG = "inputOrg";
const char* PARAM_BUCKET = "inputBucket";
const char* PARAM_WIFI_SSID = "inputWifiSsid";
const char* PARAM_WIFI_PASSWORD = "inputWifiPassword";
const char* PARAM_ACTIVE_INFLUX = "inputActiverInflux";
const char* PARAM_ACTIVE_MQTT = "inputActiverMqtt";
const char* PARAM_URL_MQTT = "inputUrlMqtt";
const char* PARAM_USER_MQTT = "inputUserMqtt";
const char* PARAM_PASSWORD_MQTT = "inputPasswordMqtt";
const char* PARAM_ABONNEMENT = "inputAbonnements";
const char* PARAM_MODEPROD = "inputModeProd";
const char* PARAM_MODEHISTSTAND = "inputModeHistStand";

/*Parameter HTML*/

// HTML web page to handle 3 input fields (inputUrl, inputToken, inputOrg, inputBucket)
const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html><head>
  <title>XKY Configuration</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script>
    function submitMessage() {
      alert("Saved value to XKY SPIFFS");
      setTimeout(function(){ document.location.reload(false); }, 5000);   
    }
  </script></head><body>
  <p><b>Configuration du module XKY</b></p>
  <p>Wifi</p>
  <form action="/get" target="hidden-form">
    SSID actuel: %inputWifiSsid% <input type="text" name="inputWifiSsid">
    <input type="submit" value="Submit" onclick="submitMessage()">
  </form><br>
  <form action="/get" target="hidden-form">
    Mot de passe actuel: %inputWifiPassword% <input type="text" name="inputWifiPassword">
    <input type="submit" value="Submit" onclick="submitMessage()">
  </form><br>
  <p>Configuration Linky</p>
  <form action="/get" target="hidden-form">
      <label for="Mode">Mode actuel: %inputModeHistStand%</label>
      <select name="inputModeHistStand" id="abon">
        <option value="Historique">Historique</option>
        <option value="Standard">Standard</option>
      </select>
      <input type="submit" value="Submit" onclick="submitMessage()">
  </form><br>
  <p>Configuration Abonnement</p>
  <form action="/get" target="hidden-form">
      <label for="abon">Abonnement actuel: %inputAbonnements%</label>
      <select name="inputAbonnements" id="abon">
        <option value="Base">Base</option>
        <option value="HCHP">HCHP</option>
        <option value="Tempo">Tempo</option>
      </select>
      <input type="submit" value="Submit" onclick="submitMessage()">
  </form><br>
  <p>Mode Production</p>
   <form action="/get" target="hidden-form">
    Activer (0= desactive/1= active) valeur actuelle: %inputModeProd% <input type="text" name="inputModeProd">
    <input type="submit" value="Submit" onclick="submitMessage()">
  </form><br>  
  <a href="influx">Configuration Influxdb</a>
  <a href="mqtt">Configuration MQTT</a>
  <a href="update">Update</a>
  <p>One For All version 1er detenteur</p>
  <iframe style="display:none" name="hidden-form"></iframe>
</body></html>)rawliteral";

// HTML web page to handle 3 input fields (inputUrl, inputToken, inputOrg, inputBucket)
const char influx_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html><head>
  <title>XKY Configuration</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script>
    function submitMessage() {
      alert("Saved value to XKY SPIFFS");
      setTimeout(function(){ document.location.reload(false); }, 5000);   
    }
  </script></head><body>
  <p><b>Configuration du module XKY</b></p>
  <p>Server InfluxDB</p>
  <form action="/get" target="hidden-form">
    Activer (0= desactive/1= active) valeur actuelle: %inputActiverInflux% <input type="text" name="inputActiverInflux">
    <input type="submit" value="Submit" onclick="submitMessage()">
  </form><br>
  <form action="/get" target="hidden-form">
    Url actuelle: %inputUrl% <input type="text" name="inputUrl">
    <input type="submit" value="Submit" onclick="submitMessage()">
  </form><br>
  <form action="/get" target="hidden-form">
    Token actuel: %inputToken% <input type="text " name="inputToken">
    <input type="submit" value="Submit" onclick="submitMessage()">
  </form><br>
  <form action="/get" target="hidden-form">
    Organisation actuelle: %inputOrg% <input type="text " name="inputOrg">
    <input type="submit" value="Submit" onclick="submitMessage()">
  </form>
  <br>
  <form action="/get" target="hidden-form">
    Bucket actuel: %inputBucket% <input type="text " name="inputBucket">
    <input type="submit" value="Submit" onclick="submitMessage()">
  </form>
  <br>
  <a href="index">Configuration Generale</a>
  <a href="mqtt">Configuration MQTT</a>
  <a href="update">Update</a>
  <p>One For All version 1er detenteur</p>
  <iframe style="display:none" name="hidden-form"></iframe>
</body></html>)rawliteral";

// HTML web page to handle 3 input fields (inputUrl, inputToken, inputOrg, inputBucket)
const char mqtt_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html><head>
  <title>XKY Configuration</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script>
    function submitMessage() {
      alert("Saved value to XKY SPIFFS");
      setTimeout(function(){ document.location.reload(false); }, 5000);   
    }
  </script></head><body>
  <p><b>Configuration du module XKY</b></p>
  <p>Server MQTT</p>
  <form action="/get" target="hidden-form">
    Activer (0= desactive/1= active) valeur actuelle: %inputActiverMqtt% <input type="text" name="inputActiverMqtt">
    <input type="submit" value="Submit" onclick="submitMessage()">
  </form><br>
  <form action="/get" target="hidden-form">
    Url actuelle: %inputUrlMqtt% <input type="text" name="inputUrlMqtt">
    <input type="submit" value="Submit" onclick="submitMessage()">
  </form><br>
  <form action="/get" target="hidden-form">
    User actuel: %inputUserMqtt% <input type="text " name="inputUserMqtt">
    <input type="submit" value="Submit" onclick="submitMessage()">
  </form><br>
  <form action="/get" target="hidden-form">
    Password actuel: %inputPasswordMqtt% <input type="text " name="inputPasswordMqtt">
    <input type="submit" value="Submit" onclick="submitMessage()">
  </form>
  <br>  
  <a href="index">Configuration Generale</a>
  <a href="influx">Configuration InfluxDB</a>
  <a href="update">Update</a>
  <p>One For All version 1er detenteur</p>
  <iframe style="display:none" name="hidden-form"></iframe>
</body></html>)rawliteral";

void notFound(AsyncWebServerRequest *request) {
  request->send(404, "text/plain", "Not found");
}

String readFile(fs::FS &fs, const char * path){
  Serial.printf("Reading file: %s\r\n", path);
  File file = fs.open(path, "r");
  if(!file || file.isDirectory()){
    Serial.println("- empty file or failed to open file");
    return String();
  }
  Serial.println("- read from file:");
  String fileContent;
  while(file.available()){
    fileContent+=String((char)file.read());
  }
  file.close();
  Serial.println(fileContent);
  return fileContent;
}

void writeFile(fs::FS &fs, const char * path, const char * message){
  Serial.printf("Writing file: %s\r\n", path);
  File file = fs.open(path, "w");
  if(!file){
    Serial.println("- failed to open file for writing");
    return;
  }
  if(file.print(message)){
    Serial.println("- file written");
  } else {
    Serial.println("- write failed");
  }
  file.close();
}

// Replaces placeholder with stored values
String processor(const String& var){
  //Serial.println(var);
  if(var == "inputUrl"){
    return INFLUXDB_URL;
  }
  else if(var == "inputToken"){
    return INFLUXDB_TOKEN;
  }
  else if(var == "inputOrg"){
    return INFLUXDB_ORG;
  }
  else if(var == "inputBucket"){
    return INFLUXDB_BUCKET;
  }
  else if(var == "inputWifiSsid"){
    return wifi_ssid;
  }
  else if(var == "inputWifiPassword"){
    return wifi_password;
  }
  else if(var == "inputActiverInflux"){
    return String(useinflux);
  }
  else if(var == "inputActiverMqtt"){
    return String(usemqtt);
  }
  else if(var == "inputUrlMqtt"){
    return mqtt_server;
  }
  else if(var == "inputUserMqtt"){
    return mqtt_user;
  }
  else if(var == "inputPasswordMqtt"){
    return mqtt_password;
  }
  else if(var == "inputAbonnements"){
    return readFile(SPIFFS, "/inputAbonnements.txt");
  }
  else if(var == "inputModeProd"){
    return String(modeprod);
  }
  else if(var == "inputModeHistStand"){
    return readFile(SPIFFS, "/inputModeHistStand.txt");
  }
  return String();
}

String macToStr(const uint8_t* mac)
{
  String result;
  for (int i = 0; i < 6; ++i) {
    result += String(mac[i], 16);
    if (i < 5)
      result += ':';
  }
  return result;
}

String composeClientID() {
  uint8_t mac[6];
  //WiFi.macAddress(mac);
  String clientId;
  clientId += "esp-";
  clientId += "00:AA:BB:CC:DE:04";
  return clientId;
}

//Reconnexion
void reconnect() {
  //Boucle jusqu'à obtenur une reconnexion
  mqttproblem = 0;
  while (!client.connected()) {
    Serial.print("Connexion au serveur MQTT...");
    String clientnumber = composeClientID();
    if (client.connect(clientnumber.c_str(), mqtt_user.c_str(), mqtt_password.c_str())) {
      Serial.println("OK");
    } 
    else {
      Serial.print("KO, erreur : "); 
      Serial.print(client.state());
      Serial.println(" On attend 1 secondes avant de recommencer");
      delay(1000);
      nbtrymqttcon = nbtrymqttcon +1;
      if (nbtrymqttcon == maxtrymqttcon) {
        mqttproblem = 1;
        break;
      }
    }
  }
}

#include "linkyhist.h"
#include "linky.h"

/* ======================================================================
  Function: Setup
  Purpose : Configuration de la voie, connexion Ethernet ou Wi-Fi et MQTT
  Input   : -
  Output  : -
  Comments: - sans DeepSleep
  ====================================================================== */
void setup() { 
  
  Serial.begin(9600);     //  sortie moniteur              
  delay(500);
  Serial.println(" ");
  Serial.println(F("Boot: version Linky_standart 9600"));
  Serial.println(ESP.getFullVersion());
  delay(1000);

//-------------- Ethernet --------------
  SPI.begin();
  SPI.setBitOrder(MSBFIRST);
  SPI.setDataMode(SPI_MODE0);
  SPI.setFrequency(4000000);

  delay(1000);
  eth.setDefault(); // use ethernet for default route
  int present = eth.begin(mac);
  if (!present) {
    Serial.println("no ethernet hardware present");
    while(1);
  }
  
  Serial.println("connecting ethernet");
  while (!eth.connected()) {
    Serial.print(".");
    delay(1000);
  }
  Serial.println();
  Serial.print("ethernet ip address: ");
  Serial.println(eth.localIP());
  Serial.print("ethernet subnetMask: ");
  Serial.println(eth.subnetMask());
  Serial.print("ethernet gateway: ");
  Serial.println(eth.gatewayIP());

  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/html", index_html, processor);
  });

  server.on("/index", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/html", index_html, processor);
  });

  server.on("/influx", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/html", influx_html, processor);
  });

  server.on("/mqtt", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/html", mqtt_html, processor);
  });

  // Send a GET request to <ESP_IP>/get?inputUrl=<inputMessage>
  server.on("/get", HTTP_GET, [] (AsyncWebServerRequest *request) {
    String inputMessage;
    // GET inputUrl value on <ESP_IP>/get?inputUrl=<inputMessage>
    if (request->hasParam(PARAM_URL)) {
      inputMessage = request->getParam(PARAM_URL)->value();
      writeFile(SPIFFS, "/inputUrl.txt", inputMessage.c_str());
      INFLUXDB_URL = inputMessage.c_str();
    }
    // GET inputToken value on <ESP_IP>/get?inputToken=<inputMessage>
    else if (request->hasParam(PARAM_TOKEN)) {
      inputMessage = request->getParam(PARAM_TOKEN)->value();
      writeFile(SPIFFS, "/inputToken.txt", inputMessage.c_str());
      INFLUXDB_TOKEN = inputMessage.c_str();
    }
    // GET inputOrg value on <ESP_IP>/get?inputOrg=<inputMessage>
    else if (request->hasParam(PARAM_ORG)) {
      inputMessage = request->getParam(PARAM_ORG)->value();
      writeFile(SPIFFS, "/inputOrg.txt", inputMessage.c_str());
      INFLUXDB_ORG = inputMessage.c_str();
    }
    else if (request->hasParam(PARAM_BUCKET)) {
      inputMessage = request->getParam(PARAM_BUCKET)->value();
      writeFile(SPIFFS, "/inputBucket.txt", inputMessage.c_str());
      INFLUXDB_BUCKET = inputMessage.c_str();
    }
    else if (request->hasParam(PARAM_WIFI_SSID)) {
      inputMessage = request->getParam(PARAM_WIFI_SSID)->value();
      writeFile(SPIFFS, "/inputWifiSsid.txt", inputMessage.c_str());
      wifi_ssid= inputMessage.c_str();
    }
    else if (request->hasParam(PARAM_WIFI_PASSWORD)) {
      inputMessage = request->getParam(PARAM_WIFI_PASSWORD)->value();
      writeFile(SPIFFS, "/inputWifiPassword.txt", inputMessage.c_str());
      wifi_password= inputMessage.c_str();
    }  
    else if (request->hasParam(PARAM_ACTIVE_INFLUX)) {
      inputMessage = request->getParam(PARAM_ACTIVE_INFLUX)->value();
      writeFile(SPIFFS, "/inputActiverInflux.txt", inputMessage.c_str());
      useinflux= readFile(SPIFFS, "/inputActiverInflux.txt") == "1";
    } 
    else if (request->hasParam(PARAM_ACTIVE_MQTT)) {
      inputMessage = request->getParam(PARAM_ACTIVE_MQTT)->value();
      writeFile(SPIFFS, "/inputActiverMqtt.txt", inputMessage.c_str());
      usemqtt= readFile(SPIFFS, "/inputActiverMqtt.txt") == "1";
    } 
    else if (request->hasParam(PARAM_URL_MQTT)) {
      inputMessage = request->getParam(PARAM_URL_MQTT)->value();
      writeFile(SPIFFS, "/inputUrlMqtt.txt", inputMessage.c_str());
      mqtt_server = inputMessage.c_str();
    } 
    else if (request->hasParam(PARAM_USER_MQTT)) {
      inputMessage = request->getParam(PARAM_USER_MQTT)->value();
      writeFile(SPIFFS, "/inputUserMqtt.txt", inputMessage.c_str());
      mqtt_user = inputMessage.c_str();
    } 
    else if (request->hasParam(PARAM_PASSWORD_MQTT)) {
      inputMessage = request->getParam(PARAM_PASSWORD_MQTT)->value();
      writeFile(SPIFFS, "/inputPasswordMqtt.txt", inputMessage.c_str());
      mqtt_password = inputMessage.c_str();
    } 
    else if (request->hasParam(PARAM_ABONNEMENT)) {
      inputMessage = request->getParam(PARAM_ABONNEMENT)->value();
      writeFile(SPIFFS, "/inputAbonnements.txt", inputMessage.c_str());
    } 
    else if (request->hasParam(PARAM_MODEPROD)) {
      inputMessage = request->getParam(PARAM_MODEPROD)->value();
      writeFile(SPIFFS, "/inputModeProd.txt", inputMessage.c_str());
      modeprod = readFile(SPIFFS, "/inputModeProd.txt") == "1";
    } 
    else if (request->hasParam(PARAM_MODEHISTSTAND)) {
      inputMessage = request->getParam(PARAM_MODEHISTSTAND)->value();
      writeFile(SPIFFS, "/inputModeHistStand.txt", inputMessage.c_str());
    } 
    else {
      inputMessage = "No message sent";
    }
    Serial.println(inputMessage);
    request->send(200, "text/text", inputMessage);
  });
  server.onNotFound(notFound);
  
  AsyncElegantOTA.begin(&server);    // Start AsyncElegantOTA
  server.begin();
  Serial.println("HTTP server started");

  if(!SPIFFS.begin()){
      Serial.println("An Error has occurred while mounting SPIFFS");
      return;
    }
//  previousstate = readFile(SPIFFS, "/previousstate.txt");
  useinflux= readFile(SPIFFS, "/inputActiverInflux.txt") == "1";
  INFLUXDB_URL = readFile(SPIFFS, "/inputUrl.txt");
  INFLUXDB_TOKEN = readFile(SPIFFS, "/inputToken.txt");
  INFLUXDB_ORG = readFile(SPIFFS, "/inputOrg.txt");
  INFLUXDB_BUCKET = readFile(SPIFFS, "/inputBucket.txt");

  wifi_ssid = readFile(SPIFFS, "/inputWifiSsid.txt");
  wifi_password = readFile(SPIFFS, "/inputWifiPassword.txt");
  usemqtt= readFile(SPIFFS, "/inputActiverMqtt.txt") == "1";
  mqtt_server= readFile(SPIFFS, "/inputUrlMqtt.txt");
  mqtt_user= readFile(SPIFFS, "/inputUserMqtt.txt");
  mqtt_password= readFile(SPIFFS, "/inputPasswordMqtt.txt");
  abonnement = readFile(SPIFFS, "/inputAbonnements.txt");
  if(abonnement == "Base" || abonnement == "HCHP"){
    typeabo =0;
  }
  else if(abonnement == "Tempo"){
    typeabo =1;
  }
  linky_histstard = readFile(SPIFFS, "/inputModeHistStand.txt") == "Standard";
  modeprod = readFile(SPIFFS, "/inputModeProd.txt") == "1";
  
  if(linky_histstard==1){
    linky_setup();
    }
    
  MQAll = millis();
  adressmacesp = composeClientID();
}

//--------------------------------------- LOOP -------------------------------
void loop() {
  if(linky_histstard==0){
    linky_hist_loop();
  }
  else{
    linky_loop();
    }
}
//----------------------------------------------------------------------------
