// **********************************************************************************
// Arduino Teleinfo sample, display information on teleinfo values received
// **********************************************************************************
// Creative Commons Attrib Share-Alike License
// You are free to use/extend this library but please abide with the CC-BY-SA license:
// http://creativecommons.org/licenses/by-sa/4.0/
//
// for detailled explanation of this library see dedicated article
// https://hallard.me/libteleinfo/
//
// For any explanation about teleinfo or use, see my blog
// https://hallard.me/category/tinfo
//
// connect Teleinfo RXD pin To Arduin D3
// see schematic here https://hallard.me/demystifier-la-teleinfo/
// and dedicated article here
//
// Written by Charles-Henri Hallard (https://hallard.me)
//
// History : V1.00 2015-06-14 - First release
//
// All text above must be included in any redistribution.
//
// **********************************************************************************
//
// adaptation pour LINKY mode Standart sur ESP32

#include "Teleinfo.h"
#include "Teleinfo_cpp.h"

TInfo    tinfo; // Teleinfo object
ValueList * me_fist;
bool  ok_me_fist = false;
bool boucleok = 0;
int compttour = 0;

String output;
Point sensor("xKy");
DynamicJsonDocument JSONbuffer(2048);


// Decode des registres linky standard //
bool testABit(byte* area, byte bitNumber) {
  return area[bitNumber >> 3] & (1 << (bitNumber & 7));
}

byte asc2byte(char chr) {
  byte rVal = 0;
  if (isdigit(chr)) {
    rVal = chr - '0';
  } else if (chr >= 'A' && chr <= 'F') {
    rVal = chr + 10 - 'A';
  }
  return rVal;
}

void unHex(const char* inP, byte* outP, size_t len) {
  for (; len > 1; len -= 2) {
    byte val = asc2byte(*inP++) << 4;
    *outP++ = val | asc2byte(*inP++);
  }
}

/* ======================================================================
  Function: printUptime
  Purpose : print pseudo uptime value
  Input   : -
  Output  : -
  Comments: compteur de secondes basique sans controle de dépassement
          En plus SoftwareSerial rend le compteur de millis() totalement
          A la rue, donc la precision de ce compteur de seconde n'est
          pas fiable du tout, dont acte !!!
  ====================================================================== */
void printUptime(void)
{
  Serial.print(millis() / 1000);
  Serial.print(F("s\t"));
}

/* ======================================================================
  Function: DataCallback
  Purpose : callback when we detected new or modified data received
  Input   : linked list pointer on the concerned data
          current flags value
  Output  : -
  Comments: -
  ====================================================================== */
void DataCallback(ValueList * me, uint8_t  flags)
{
  // Show our not accurate second counter
  //printUptime();

  if (flags & TINFO_FLAGS_ADDED || flags & TINFO_FLAGS_UPDATED)
    if  ( !ok_me_fist) {
      ok_me_fist = true;
      me_fist = me;
    }

  // Display values
  Serial.print("==> ");
  Serial.print(me->name);
  if (strlen(me->name) < 4 ) Serial.print(F("\t")) ;
  Serial.print(F("\t")) ;
  if ( me->value[0] != '*') {
    Serial.print(F(" = ")) ;
    Serial.print(me->value) ;
  }
  if ( me->date[0] != '*' ) {
    Serial.print(" : ");
    Serial.print(me->date);
    DATE = me->date;
    okDATE = 1;
    compttour = compttour + 1;
  }
  if ((me->name[0] == 'A') && (me->name[1] == 'D') && (me->name[2] == 'S') && (me->name[3] == 'C')) {
    ADSC = me->value;
    okADSC = 1;
    Serial.print("ADSC:");
    Serial.println(ADSC);
  }
  else if ((me->name[0] == 'V') && (me->name[1] == 'T') && (me->name[2] == 'I') && (me->name[3] == 'C')) {
    VTIC = me->value;
    okVTIC = 1;
    Serial.print("VTIC:");
    Serial.println(VTIC);
  }
  else if ((me->name[0] == 'N') && (me->name[1] == 'G') && (me->name[2] == 'T') && (me->name[3] == 'F')) {
    NGTF = me->value;
    okNGTF = 1;
    Serial.print("NGTF:");
    Serial.println(NGTF);
  }
  else if ((me->name[0] == 'L') && (me->name[1] == 'T') && (me->name[2] == 'A') && (me->name[3] == 'R') && (me->name[4] == 'F')) {
    LTARF = me->value;
    okLTARF = 1;
    Serial.print("LTARF:");
    Serial.println(LTARF);
  }
  else if ((me->name[0] == 'E') && (me->name[1] == 'A') && (me->name[2] == 'S') && (me->name[3] == 'T')) {
    EAST = me->value;
    okEAST = 1;
    Serial.print("EAST:");
    Serial.println(EAST);
  }
  else if ((me->name[0] == 'E') && (me->name[1] == 'A') && (me->name[2] == 'S') && (me->name[3] == 'F') && (me->name[4] == '0') && (me->name[5] == '1')) {
    EASF01 = me->value;
    okEASF01 = 1;
    Serial.print("EASF01:");
    Serial.println(EASF01);
  }
  else if ((me->name[0] == 'E') && (me->name[1] == 'A') && (me->name[2] == 'S') && (me->name[3] == 'F') && (me->name[4] == '0') && (me->name[5] == '2')) {
    EASF02 = me->value;
    okEASF02 = 1;
    Serial.print("EASF02:");
    Serial.println(EASF02);
  }
  else if ((me->name[0] == 'E') && (me->name[1] == 'A') && (me->name[2] == 'S') && (me->name[3] == 'F') && (me->name[4] == '0') && (me->name[5] == '3')) {
    EASF03 = me->value;
    okEASF03 = 1;
    Serial.print("EASF03:");
    Serial.println(EASF03);
  }
  else if ((me->name[0] == 'E') && (me->name[1] == 'A') && (me->name[2] == 'S') && (me->name[3] == 'F') && (me->name[4] == '0') && (me->name[5] == '4')) {
    EASF04 = me->value;
    okEASF04 = 1;
    Serial.print("EASF04:");
    Serial.println(EASF04);
  }
  else if ((me->name[0] == 'E') && (me->name[1] == 'A') && (me->name[2] == 'S') && (me->name[3] == 'F') && (me->name[4] == '0') && (me->name[5] == '5')) {
    EASF05 = me->value;
    okEASF05 = 1;
    Serial.print("EASF05:");
    Serial.println(EASF05);
  }
  else if ((me->name[0] == 'E') && (me->name[1] == 'A') && (me->name[2] == 'S') && (me->name[3] == 'F') && (me->name[4] == '0') && (me->name[5] == '6')) {
    EASF06 = me->value;
    okEASF06 = 1;
    Serial.print("EASF06:");
    Serial.println(EASF06);
  }
  else if ((me->name[0] == 'E') && (me->name[1] == 'A') && (me->name[2] == 'S') && (me->name[3] == 'F') && (me->name[4] == '0') && (me->name[5] == '7')) {
    EASF07 = me->value;
    okEASF07 = 1;
    Serial.print("EASF07:");
    Serial.println(EASF07);
  }
  else if ((me->name[0] == 'E') && (me->name[1] == 'A') && (me->name[2] == 'S') && (me->name[3] == 'F') && (me->name[4] == '0') && (me->name[5] == '8')) {
    EASF08 = me->value;
    okEASF08 = 1;
    Serial.print("EASF08:");
    Serial.println(EASF08);
  }
  else if ((me->name[0] == 'E') && (me->name[1] == 'A') && (me->name[2] == 'S') && (me->name[3] == 'F') && (me->name[4] == '0') && (me->name[5] == '9')) {
    EASF09 = me->value;
    okEASF09 = 1;
    Serial.print("EASF09:");
    Serial.println(EASF09);
  }
  else if ((me->name[0] == 'E') && (me->name[1] == 'A') && (me->name[2] == 'S') && (me->name[3] == 'F') && (me->name[4] == '1') && (me->name[5] == '0')) {
    EASF10 = me->value;
    okEASF10 = 1;
    Serial.print("EASF10:");
    Serial.println(EASF10);
  }
  else if ((me->name[0] == 'E') && (me->name[1] == 'A') && (me->name[2] == 'S') && (me->name[3] == 'D') && (me->name[4] == '0') && (me->name[5] == '1')) {
    EASD01 = me->value;
    okEASD01 = 1;
    Serial.print("EASD01:");
    Serial.println(EASD01);
  }
  else if ((me->name[0] == 'E') && (me->name[1] == 'A') && (me->name[2] == 'S') && (me->name[3] == 'D') && (me->name[4] == '0') && (me->name[5] == '2')) {
    EASD02 = me->value;
    okEASD02 = 1;
    Serial.print("EASD02:");
    Serial.println(EASD02);
  }
  else if ((me->name[0] == 'E') && (me->name[1] == 'A') && (me->name[2] == 'S') && (me->name[3] == 'D') && (me->name[4] == '0') && (me->name[5] == '3')) {
    EASD03 = me->value;
    okEASD03 = 1;
    Serial.print("EASD03:");
    Serial.println(EASD03);
  }
  else if ((me->name[0] == 'E') && (me->name[1] == 'A') && (me->name[2] == 'S') && (me->name[3] == 'D') && (me->name[4] == '0') && (me->name[5] == '4')) {
    EASD04 = me->value;
    okEASD04 = 1;
    Serial.print("EASD04:");
    Serial.println(EASD04);
  }
  else if ((me->name[0] == 'E') && (me->name[1] == 'A') && (me->name[2] == 'I') && (me->name[3] == 'T')) {
    EAIT = me->value;
    okEAIT = 1;
    Serial.print("EAIT:");
    Serial.println(EAIT);
  }
  else if ((me->name[0] == 'E') && (me->name[1] == 'R') && (me->name[2] == 'Q') && (me->name[3] == '1')) {
    ERQ1 = me->value;
    okERQ1 = 1;
    Serial.print("ERQ1:");
    Serial.println(ERQ1);
  }
  else if ((me->name[0] == 'E') && (me->name[1] == 'R') && (me->name[2] == 'Q') && (me->name[3] == '2')) {
    ERQ2 = me->value;
    okERQ2 = 1;
    Serial.print("ERQ2:");
    Serial.println(ERQ2);
  }
  else if ((me->name[0] == 'E') && (me->name[1] == 'R') && (me->name[2] == 'Q') && (me->name[3] == '3')) {
    ERQ3 = me->value;
    okERQ3 = 1;
    Serial.print("ERQ3:");
    Serial.println(ERQ3);
  }
  else if ((me->name[0] == 'E') && (me->name[1] == 'R') && (me->name[2] == 'Q') && (me->name[3] == '4')) {
    ERQ4 = me->value;
    okERQ4 = 1;
    Serial.print("ERQ4:");
    Serial.println(ERQ4);
  }
  else if ((me->name[0] == 'I') && (me->name[1] == 'R') && (me->name[2] == 'M') && (me->name[3] == 'S') && (me->name[4] == '1')) {
    IRMS1 = me->value;
    okIRMS1 = 1;
    Serial.print("IRMS1:");
    Serial.println(IRMS1);
  }
  else if (
    (me->name[0] == 'I') && (me->name[1] == 'R') && (me->name[2] == 'M') && (me->name[3] == 'S') && (me->name[4] == '2')) {
    IRMS2 = me->value;
    okIRMS2 = 1;
    Serial.print("IRMS2:");
    Serial.println(IRMS2);
  }
  else if ((me->name[0] == 'I') && (me->name[1] == 'R') && (me->name[2] == 'M') && (me->name[3] == 'S') && (me->name[4] == '3')) {
    IRMS3 = me->value;
    okIRMS3 = 1;
    Serial.print("IRMS3:");
    Serial.println(IRMS3);
  }
  else if ((me->name[0] == 'U') && (me->name[1] == 'R') && (me->name[2] == 'M') && (me->name[3] == 'S') && (me->name[4] == '1')) {
    URMS1 = me->value;
    okURMS1 = 1;
    Serial.print("URMS1:");
    Serial.println(URMS1);
  }
  else if ((me->name[0] == 'U') && (me->name[1] == 'R') && (me->name[2] == 'M') && (me->name[3] == 'S') && (me->name[4] == '2')) {
    URMS2 = me->value;
    okURMS2 = 1;
    Serial.print("URMS2:");
    Serial.println(URMS2);
  }
  else if ((me->name[0] == 'U') && (me->name[1] == 'R') && (me->name[2] == 'M') && (me->name[3] == 'S') && (me->name[4] == '3')) {
    URMS3 = me->value;
    okURMS3 = 1;
    Serial.print("URMS3:");
    Serial.println(URMS3);
  }
  else if ((me->name[0] == 'P') && (me->name[1] == 'R') && (me->name[2] == 'E') && (me->name[3] == 'F')) {
    PREF = me->value;
    okPREF = 1;
    Serial.print("PREF:");
    Serial.println(PREF);
  }
  else if ((me->name[0] == 'P') && (me->name[1] == 'C') && (me->name[2] == 'O') && (me->name[3] == 'U') && (me->name[4] == 'P')) {
    PCOUP = me->value;
    okPCOUP = 1;
    Serial.print("PCOUP:");
    Serial.println(PCOUP);
  }
  else if ((me->name[0] == 'S') && (me->name[1] == 'I') && (me->name[2] == 'N') && (me->name[3] == 'S') && (me->name[4] == 'T') && (me->name[5] == 'S') && (me->name[6] == '1')) {
    SINSTS1 = me->value;
    okSINSTS1 = 1;
    Serial.print("SINSTS1:");
    Serial.println(SINSTS1);
  }
  else if ((me->name[0] == 'S') && (me->name[1] == 'I') && (me->name[2] == 'N') && (me->name[3] == 'S') && (me->name[4] == 'T') && (me->name[5] == 'S') && (me->name[6] == '2')) {
    SINSTS2 = me->value;
    okSINSTS2 = 1;
    Serial.print("SINSTS2:");
    Serial.println(SINSTS2);
  }
  else if ((me->name[0] == 'S') && (me->name[1] == 'I') && (me->name[2] == 'N') && (me->name[3] == 'S') && (me->name[4] == 'T') && (me->name[5] == 'S') && (me->name[6] == '3')) {
    SINSTS3 = me->value;
    okSINSTS3 = 1;
    Serial.print("SINSTS3:");
    Serial.println(SINSTS3);
  }
  else if ((me->name[0] == 'S') && (me->name[1] == 'I') && (me->name[2] == 'N') && (me->name[3] == 'S') && (me->name[4] == 'T') && (me->name[5] == 'S')) {
    SINSTS = me->value;
    okSINSTS = 1;
    Serial.print("SINSTS:");
    Serial.println(SINSTS);
  }
  else if ((me->name[0] == 'S') && (me->name[1] == 'M') && (me->name[2] == 'A') && (me->name[3] == 'X') && (me->name[4] == 'S') && (me->name[5] == 'N') && (me->name[6] == '1') && (me->name[7] == '-') && (me->name[8] == '1')) {
    SMAXSN1_1 = me->value;
    okSMAXSN1_1 = 1;
    Serial.print("SMAXSN1_1:");
    Serial.println(SMAXSN1_1);
  }
  else if ((me->name[0] == 'S') && (me->name[1] == 'M') && (me->name[2] == 'A') && (me->name[3] == 'X') && (me->name[4] == 'S') && (me->name[5] == 'N') && (me->name[6] == '2') && (me->name[7] == '-') && (me->name[8] == '1')) {
    SMAXSN2_1 = me->value;
    okSMAXSN2_1 = 1;
    Serial.print("SMAXSN2_1:");
    Serial.println(SMAXSN2_1);
  }
  else if ((me->name[0] == 'S') && (me->name[1] == 'M') && (me->name[2] == 'A') && (me->name[3] == 'X') && (me->name[4] == 'S') && (me->name[5] == 'N') && (me->name[6] == '3') && (me->name[7] == '-') && (me->name[8] == '1')) {
    SMAXSN3_1 = me->value;
    okSMAXSN3_1 = 1;
    Serial.print("SMAXSN3_1:");
    Serial.println(SMAXSN3_1);
  }
  else if ((me->name[0] == 'S') && (me->name[1] == 'M') && (me->name[2] == 'A') && (me->name[3] == 'X') && (me->name[4] == 'S') && (me->name[5] == 'N') && (me->name[6] == '-') && (me->name[7] == '1')) {
    SMAXSN_1 = me->value;
    okSMAXSN_1 = 1;
    Serial.print("SMAXSN_1:");
    Serial.println(SMAXSN_1);
  }
  else if ((me->name[0] == 'S') && (me->name[1] == 'M') && (me->name[2] == 'A') && (me->name[3] == 'X') && (me->name[4] == 'S') && (me->name[5] == 'N') && (me->name[6] == '1')) {
    SMAXSN1 = me->value;
    okSMAXSN1 = 1;
    Serial.print("SMAXSN1:");
    Serial.println(SMAXSN1);
  }
  else if ((me->name[0] == 'S') && (me->name[1] == 'M') && (me->name[2] == 'A') && (me->name[3] == 'X') && (me->name[4] == 'S') && (me->name[5] == 'N') && (me->name[6] == '2')) {
    SMAXSN2 = me->value;
    okSMAXSN2 = 1;
    Serial.print("SMAXSN2:");
    Serial.println(SMAXSN2);
  }
  else if ((me->name[0] == 'S') && (me->name[1] == 'M') && (me->name[2] == 'A') && (me->name[3] == 'X') && (me->name[4] == 'S') && (me->name[5] == 'N') && (me->name[6] == '3')) {
    SMAXSN3 = me->value;
    okSMAXSN3 = 1;
    Serial.print("SMAXSN3:");
    Serial.println(SMAXSN3);
  }
  else if ((me->name[0] == 'S') && (me->name[1] == 'M') && (me->name[2] == 'A') && (me->name[3] == 'X') && (me->name[4] == 'S') && (me->name[5] == 'N')) {
    SMAXSN = me->value;
    okSMAXSN = 1;
    Serial.print("SMAXSN:");
    Serial.println(SMAXSN);
  }
  else if ((me->name[0] == 'S') && (me->name[1] == 'I') && (me->name[2] == 'N') && (me->name[3] == 'S') && (me->name[4] == 'T') && (me->name[5] == 'I')) {
    SINSTI = me->value;
    okSINSTI = 1;
    Serial.print("SINSTI:");
    Serial.println(SINSTI);
  }
  else if ((me->name[0] == 'S') && (me->name[1] == 'M') && (me->name[2] == 'A') && (me->name[3] == 'X') && (me->name[4] == 'I') && (me->name[5] == 'N') && (me->name[6] == '-') && (me->name[7] == '1')) {
    SMAXIN_1 = me->value;
    okSMAXIN_1 = 1;
    Serial.print("SMAXIN_1:");
    Serial.println(SMAXIN_1);
  }
  else if ((me->name[0] == 'S') && (me->name[1] == 'M') && (me->name[2] == 'A') && (me->name[3] == 'X') && (me->name[4] == 'I') && (me->name[5] == 'N')) {
    SMAXIN = me->value;
    okSMAXIN = 1;
    Serial.print("SMAXIN:");
    Serial.println(SMAXIN);
  }
  else if ((me->name[0] == 'C') && (me->name[1] == 'C') && (me->name[2] == 'A') && (me->name[3] == 'S') && (me->name[4] == 'N') && (me->name[5] == '-') && (me->name[6] == '1')) {
    CCASN_1 = me->value;
    okCCASN_1 = 1;
    Serial.print("CCASN_1:");
    Serial.println(CCASN_1);
  }
  else if ((me->name[0] == 'C') && (me->name[1] == 'C') && (me->name[2] == 'A') && (me->name[3] == 'S') && (me->name[4] == 'N')) {
    CCASN = me->value;
    okCCASN = 1;
    Serial.print("CCASN:");
    Serial.println(CCASN);
  }
  else if ((me->name[0] == 'C') && (me->name[1] == 'C') && (me->name[2] == 'A') && (me->name[3] == 'I') && (me->name[4] == 'N') && (me->name[5] == '-') && (me->name[6] == '1')) {
    CCAIN_1 = me->value;
    okCCAIN_1 = 1;
    Serial.print("CCAIN_1:");
    Serial.println(CCAIN_1);
  }
  else if ((me->name[0] == 'C') && (me->name[1] == 'C') && (me->name[2] == 'A') && (me->name[3] == 'I') && (me->name[4] == 'N')) {
    CCAIN = me->value;
    okCCAIN = 1;
    Serial.print("CCAIN:");
    Serial.println(CCAIN);
  }
  else if ((me->name[0] == 'U') && (me->name[1] == 'M') && (me->name[2] == 'O') && (me->name[3] == 'Y') && (me->name[4] == '1')) {
    UMOY1 = me->value;
    okUMOY1 = 1;
    Serial.print("UMOY1:");
    Serial.println(UMOY1);
  }
  else if ((me->name[0] == 'U') && (me->name[1] == 'M') && (me->name[2] == 'O') && (me->name[3] == 'Y') && (me->name[4] == '2')) {
    UMOY2 = me->value;
    okUMOY2 = 1;
    Serial.print("UMOY2:");
    Serial.println(UMOY2);
  }
  else if ((me->name[0] == 'U') && (me->name[1] == 'M') && (me->name[2] == 'O') && (me->name[3] == 'Y') && (me->name[4] == '3')) {
    UMOY3 = me->value;
    okUMOY3 = 1;
    Serial.print("UMOY3:");
    Serial.println(UMOY3);
  }
  else if ((me->name[0] == 'S') && (me->name[1] == 'T') && (me->name[2] == 'G') && (me->name[3] == 'E')) {
    STGE = me->value;
    okSTGE = 1;
    Serial.print("STGE:");
    Serial.println(STGE);
  }
  else if ((me->name[0] == 'D') && (me->name[1] == 'P') && (me->name[2] == 'M') && (me->name[3] == '1')) {
    DPM1 = me->value;
    okDPM1 = 1;
    Serial.print("DPM1:");
    Serial.println(DPM1);
  }
  else if ((me->name[0] == 'F') && (me->name[1] == 'P') && (me->name[2] == 'M') && (me->name[3] == '1')) {
    FPM1 = me->value;
    okFPM1 = 1;
    Serial.print("FPM1:");
    Serial.println(FPM1);
  }
  else if ((me->name[0] == 'D') && (me->name[1] == 'P') && (me->name[2] == 'M') && (me->name[3] == '2')) {
    DPM2 = me->value;
    okDPM2 = 1;
    Serial.print("DPM2:");
    Serial.println(DPM2);
  }
  else if ((me->name[0] == 'F') && (me->name[1] == 'P') && (me->name[2] == 'M') && (me->name[3] == '2')) {
    FPM2 = me->value;
    okFPM2 = 1;
    Serial.print("FPM2:");
    Serial.println(FPM2);
  }
  else if ((me->name[0] == 'D') && (me->name[1] == 'P') && (me->name[2] == 'M') && (me->name[3] == '3')) {
    DPM3 = me->value;
    okDPM3 = 1;
    Serial.print("DPM3:");
    Serial.println(DPM3);
  }
  else if ((me->name[0] == 'F') && (me->name[1] == 'P') && (me->name[2] == 'M') && (me->name[3] == '3')) {
    FPM3 = me->value;
    okFPM3 = 1;
    Serial.print("FPM3:");
    Serial.println(FPM3);
  }
  else if ((me->name[0] == 'M') && (me->name[1] == 'S') && (me->name[2] == 'G') && (me->name[3] == '1')) {
    MSG1 = me->value;
    okMSG1 = 1;
    Serial.print("MSG1:");
    Serial.println(MSG1);
  }
  else if ((me->name[0] == 'M') && (me->name[1] == 'S') && (me->name[2] == 'G') && (me->name[3] == '2')) {
    MSG2 = me->value;
    okMSG2 = 1;
    Serial.print("MSG2:");
    Serial.println(MSG2);
  }
  else if ((me->name[0] == 'P') && (me->name[1] == 'R') && (me->name[2] == 'M')) {
    PRM = me->value;
    okPRM = 1;
    Serial.print("PRM:");
    Serial.println(PRM);
    boucleok = 1;
  }
  else if ((me->name[0] == 'R') && (me->name[1] == 'E') && (me->name[2] == 'L')) {
    RELAIS = me->value;
    okRELAIS = 1;
    Serial.print("RELAIS:");
    Serial.println(RELAIS);
  }
  else if ((me->name[0] == 'N') && (me->name[1] == 'T') && (me->name[2] == 'A') && (me->name[3] == 'R') && (me->name[4] == 'F')) {
    NTARF = me->value;
    okNTARF = 1;
    Serial.print("NTARF:");
    Serial.println(NTARF);
  }
  else if ((me->name[0] == 'N') && (me->name[1] == 'J') && (me->name[2] == 'O') && (me->name[3] == 'U') && (me->name[4] == 'R') && (me->name[5] == 'F') && (me->name[7] == '1')) {
    NJOURF1 = me->value;
    okNJOURF1 = 1;
    Serial.print("NJOURF1:");
    Serial.println(NJOURF1);
  }
  else if ((me->name[0] == 'N') && (me->name[1] == 'J') && (me->name[2] == 'O') && (me->name[3] == 'U') && (me->name[4] == 'R') && (me->name[5] == 'F')) {
    NJOURF = me->value;
    okNJOURF = 1;
    Serial.print("NJOURF:");
    Serial.println(NJOURF);
  }
  else if ((me->name[0] == 'P') && (me->name[1] == 'J') && (me->name[2] == 'O') && (me->name[3] == 'U') && (me->name[4] == 'R') && (me->name[5] == 'F') && (me->name[7] == '1')) {
    PJOURF1 = me->value;
    okPJOURF1 = 1;
    Serial.print("PJOURF1:");
    Serial.println(PJOURF1);
  }
  else if ((me->name[0] == 'P') && (me->name[1] == 'P') && (me->name[2] == 'O') && (me->name[3] == 'I') && (me->name[4] == 'N') && (me->name[5] == 'T') && (me->name[7] == 'E')) {
    PPOINTE = me->value;
    okPPOINTE = 1;
    Serial.print("PPOINTE:");
    Serial.println(PPOINTE);
  }
  Serial.println("");
}

/* ======================================================================
  Function: sendJSON
  Purpose : dump teleinfo values on serial
  Input   : linked list pointer on the concerned data
          true to dump all values, false for only modified ones
  Output  : -
  Comments: -
  ====================================================================== */
void sendAll()
{
  ValueList * me;
  me = me_fist;                                   // reprendre le 1er de la chaine
  // Got at least one ?
  if (me) {

    Serial.print(F("\"all\":"));
    Serial.print(me->name) ;
    Serial.print(F("\t")) ;
    if ( me->value[0] != '*') {
      Serial.print(F(" = ")) ;
      Serial.print(me->value) ;
    }
    if ( me->date[0] != '*') {
      Serial.print(" : ");
      Serial.print(me->date);
    }
    Serial.println("");

    // Loop thru the node
    while (me->next) {

      // go to next node
      me = me->next;                                // suivant
      //sprintf(msg, "%s=%s", me->name, me->value );
      //Mqtt_emettre(me->name, me->value);

      Serial.print(F("\"all\":"));
      Serial.print(me->name) ;
      //if (strlen(me->name) < 4 ) Serial.print(F("\t")) ;
      Serial.print(F("\t")) ;
      if ( me->value[0] != '*') {
        Serial.print(F(" = ")) ;
        Serial.print(me->value) ;
      }
      if (me->date[0] != '*') {
        Serial.print(" : ");
        Serial.print(me->date);
      }
      Serial.println("");

    }
  }
}

/* ======================================================================
  Function: setup
  Purpose : Setup I/O and other one time startup stuff
  Input   : -
  Output  : -
  Comments: -
  ====================================================================== */
void linky_setup()
{

  Serial.println(F("======== Setup Teleinfo Linky ======="));
  delay(200);

  Serial.begin(9600);
  // Init teleinfo
  tinfo.init();

  // Attacher les callback dont nous avons besoin
  // pour cette demo, ici attach data
  tinfo.attachData(DataCallback);

}

void DataformatComPart1() {
  timedecode = millis();
  sensor.clearFields();

  sensor.addField("ModeTic", "Standard");
  JSONbuffer["ModeTic"] = String("Standard");
  sensor.addField("FWVersion", FWVersion);
  JSONbuffer["FWVersion"] = String(FWVersion);
  sensor.addField("BoardVersion", BoardVersion);
  JSONbuffer["BoardVersion"] = String(BoardVersion);

  if (okADSC) {
    sensor.addField("ADSC", String(ADSC));
    JSONbuffer["ADSC"] = String(ADSC);
  }
  if (okVTIC) {
    sensor.addField("VTIC", String(VTIC));
    JSONbuffer["VTIC"] = String(VTIC);
  }
  if (okDATE) {
    sensor.addField("DATE", String(DATE));
    JSONbuffer["DATE"] = String(DATE);
  }
  if (okNGTF) {
    sensor.addField("NGTF", String(NGTF));
    JSONbuffer["NGTF"] = String(NGTF);
  }
  if (okLTARF) {
    sensor.addField("LTARF", String(LTARF));
    JSONbuffer["LTARF"] = String(LTARF);
    sensor.addField("OPTARIF", String(LTARF));
    sensor.addField("PTEC", String(LTARF));
    JSONbuffer["PTEC"] = String(PTEC);
  }
  if (okEAST) {
    sensor.addField("EAST", atoi(EAST));
    sensor.addField("BASE", atoi(EAST));
    JSONbuffer["EAST"] = String(EAST);
  }
  if (okEASF01) {
    sensor.addField("EASF01", atoi(EASF01));
    JSONbuffer["EASF01"] = String(EASF01);
  }
  if (okEASF02) {
    sensor.addField("EASF02", atoi(EASF02));
    JSONbuffer["EASF02"] = String(EASF02);
  }
  if (okEASF03) {
    sensor.addField("EASF03", atoi(EASF03));
    JSONbuffer["EASF03"] = String(EASF03);
  }
  if (okEASF04) {
    sensor.addField("EASF04", atoi(EASF04));
    JSONbuffer["EASF04"] = String(EASF04);
  }
  if (okEASF05) {
    sensor.addField("EASF05", atoi(EASF05));
    JSONbuffer["EASF05"] = String(EASF05);
  }
  if (okEASF06) {
    sensor.addField("EASF06", atoi(EASF06));
    JSONbuffer["EASF06"] = String(EASF06);
  }
  if (okEASF07) {
    sensor.addField("EASF07", atoi(EASF07));
    JSONbuffer["EASF07"] = String(EASF07);
  }
  if (okEASF08) {
    sensor.addField("EASF08", atoi(EASF08));
    JSONbuffer["EASF08"] = String(EASF08);
  }
  if (okEASF09) {
    sensor.addField("EASF09", atoi(EASF09));
    JSONbuffer["EASF09"] = String(EASF09);
  }
  if (okEASF10) {
    sensor.addField("EASF10", atoi(EASF10));
    JSONbuffer["EASF10"] = String(EASF10);
  }
  if (okEASD01) {
    sensor.addField("EASD01", atoi(EASD01));
    JSONbuffer["EASD01"] = String(EASD01);
  }
  if (okEASD02) {
    sensor.addField("EASD02", atoi(EASD02));
    JSONbuffer["EASD02"] = String(EASD02);
  }
  if (okEASD03) {
    sensor.addField("EASD03", atoi(EASD03));
    JSONbuffer["EASD03"] = String(EASD03);
  }
  if (okEASD04) {
    sensor.addField("EASD04", atoi(EASD04));
    JSONbuffer["EASD04"] = String(EASD04);
  }
  if (okERQ1) {
    sensor.addField("ERQ1", atoi(ERQ1));
    JSONbuffer["ERQ1"] = String(ERQ1);
  }
  if (okERQ2) {
    sensor.addField("ERQ2", atoi(ERQ2));
    JSONbuffer["ERQ2"] = String(ERQ2);
  }
  if (okERQ3) {
    sensor.addField("ERQ3", atoi(ERQ3));
    JSONbuffer["ERQ3"] = String(ERQ3);
  }
  if (okERQ4) {
    sensor.addField("ERQ4", atoi(ERQ4));
    JSONbuffer["ERQ4"] = String(ERQ4);
  }
  if (okEAIT) {
    sensor.addField("EAIT", atoi(EAIT));
    JSONbuffer["EAIT"] = String(EAIT);
  }
  if (okIRMS1) {
    sensor.addField("IRMS1", atoi(IRMS1));
    JSONbuffer["IRMS1"] = String(IRMS1);
  }
  if (okIRMS2) {
    sensor.addField("IRMS2", atoi(IRMS2));
    JSONbuffer["IRMS2"] = String(IRMS2);
  }
  if (okIRMS3) {
    sensor.addField("IRMS3", atoi(IRMS3));
    JSONbuffer["IRMS3"] = String(IRMS3);
  }
  if (okURMS1) {
    sensor.addField("URMS1", atoi(URMS1));
    JSONbuffer["URMS1"] = String(URMS1);
  }
  if (okURMS2) {
    sensor.addField("URMS2", atoi(URMS2));
    JSONbuffer["URMS2"] = String(URMS2);
  }
  if (okURMS3) {
    sensor.addField("URMS3", atoi(URMS3));
    JSONbuffer["URMS3"] = String(URMS3);
  }
  if (okPREF) {
    sensor.addField("PREF", atoi(PREF));
    JSONbuffer["PREF"] = String(PREF);
  }
  if (okPCOUP) {
    sensor.addField("PCOUP", atoi(PCOUP));
    JSONbuffer["PCOUP"] = String(PCOUP);
  }
  if (okSINSTS) {
    sensor.addField("SINSTS", atoi(SINSTS));
    JSONbuffer["SINSTS"] = String(SINSTS);
    sensor.addField("PAPP", atoi(SINSTS));
  }
  if (okSINSTS1) {
    sensor.addField("SINSTS1", atoi(SINSTS1));
    JSONbuffer["SINSTS1"] = String(SINSTS1);
  }
  if (okSINSTS2) {
    sensor.addField("SINSTS2", atoi(SINSTS2));
    JSONbuffer["SINSTS2"] = String(SINSTS2);
  }
  if (okSINSTS3) {
    sensor.addField("SINSTS3", atoi(SINSTS3));
    JSONbuffer["SINSTS3"] = String(SINSTS3);
  }
  if (okSMAXSN) {
    sensor.addField("SMAXSN", atoi(SMAXSN));
    JSONbuffer["SMAXSN"] = String(SMAXSN);
  }
  if (okSMAXSN1) {
    sensor.addField("SMAXSN1", atoi(SMAXSN1));
    JSONbuffer["SMAXSN1"] = String(SMAXSN1);
  }
  if (okSMAXSN2) {
    sensor.addField("SMAXSN2", atoi(SMAXSN2));
    JSONbuffer["SMAXSN2"] = String(SMAXSN2);
  }
  if (okSMAXSN3) {
    sensor.addField("SMAXSN3", atoi(SMAXSN3));
    JSONbuffer["SMAXSN3"] = String(SMAXSN3);
  }
  if (okSMAXSN_1) {
    sensor.addField("SMAXSN-1", atoi(SMAXSN_1));
    JSONbuffer["SMAXSN-1"] = String(SMAXSN_1);
  }
  if (okSMAXSN1_1) {
    sensor.addField("SMAXSN1-1", atoi(SMAXSN1_1));
    JSONbuffer["SMAXSN1-1"] = String(SMAXSN1_1);
  }
  if (okSMAXSN2_1) {
    sensor.addField("SMAXSN2-1", atoi(SMAXSN2_1));
    JSONbuffer["SMAXSN2-1"] = String(SMAXSN2_1);
  }
  if (okSMAXSN3_1) {
    sensor.addField("SMAXSN3-1", atoi(SMAXSN3_1));
    JSONbuffer["SMAXSN3-1"] = String(SMAXSN3_1);
  }
  if (okSINSTI) {
    sensor.addField("SINSTI", atoi(SINSTI));
    JSONbuffer["SINSTI"] = String(SINSTI);
  }
  if (okSMAXIN) {
    sensor.addField("SMAXIN", atoi(SMAXIN));
    JSONbuffer["SMAXIN"] = String(SMAXIN);
  }
  if (okSMAXIN_1) {
    sensor.addField("SMAXIN-1", atoi(SMAXIN_1));
    JSONbuffer["SMAXIN-1"] = String(SMAXIN_1);
  }
  if (okCCAIN) {
    sensor.addField("CCAIN", atoi(CCAIN));
    JSONbuffer["CCAIN"] = String(CCAIN);
  }
  if (okCCAIN_1) {
    sensor.addField("CCAIN-1", atoi(CCAIN_1));
    JSONbuffer["CCAIN-1"] = String(CCAIN_1);
  }
  if (okCCASN_1) {
    sensor.addField("CCASN-1", atoi(CCASN_1));
    JSONbuffer["CCASN-1"] = String(CCASN_1);
  }
  if (okCCASN) {
    sensor.addField("CCASN", atoi(CCASN));
    JSONbuffer["CCASN"] = String(CCASN);
  }
  if (okUMOY1) {
    sensor.addField("UMOY1", atoi(UMOY1));
    JSONbuffer["UMOY1"] = String(UMOY1);
  }
  if (okUMOY2) {
    sensor.addField("UMOY2", atoi(UMOY2));
    JSONbuffer["UMOY2"] = String(UMOY2);
  }
  if (okUMOY3) {
    sensor.addField("UMOY3", atoi(UMOY3));
    JSONbuffer["UMOY3"] = String(UMOY3);
  }
  if (okSTGE) {
    sensor.addField("STGE", String(STGE));
    JSONbuffer["STGE"] = String(STGE);
    stgetrame = String(STGE);
    inHex[0] = stgetrame[6];
    inHex[1] = stgetrame[7];
    inHex[2] = stgetrame[4];
    inHex[3] = stgetrame[5];
    inHex[4] = stgetrame[2];
    inHex[5] = stgetrame[3];
    inHex[6] = stgetrame[0];
    inHex[7] = stgetrame[1];
    unHex(inHex, outData, strlen(inHex));
    EContactSec = testABit(outData, 0) * 1;
    sensor.addField("EContactSec", EContactSec);
    JSONbuffer["EContactSec"] = String(EContactSec);
    EOrganeCoupure = testABit(outData, 1) * 1 + testABit(outData, 2) * 2 + testABit(outData, 3) * 4;
    sensor.addField("EOrganeCoupure", EOrganeCoupure);
    JSONbuffer["EOrganeCoupure"] = String(EOrganeCoupure);
    ECache = testABit(outData, 4) * 1;
    sensor.addField("ECache", ECache);
    JSONbuffer["ECache"] = String(ECache);
    // Serial.println(testABit(outData, 5) * 1);
    ESurtension = testABit(outData, 6) * 1;
    sensor.addField("ESurtension", ESurtension);
    JSONbuffer["ESurtension"] = String(ESurtension);
    EDepassement = testABit(outData, 7) * 1;
    sensor.addField("EDepassement", EDepassement);
    JSONbuffer["EDepassement"] = String(EDepassement);
    EFonctionnement = testABit(outData, 8) * 1;
    sensor.addField("EFonctionnement", EFonctionnement);
    JSONbuffer["EFonctionnement"] = String(EFonctionnement);
    ESensEnergie = testABit(outData, 9) * 1;
    sensor.addField("ESensEnergie", ESensEnergie);
    JSONbuffer["ESensEnergie"] = String(ESensEnergie);
    ETarifFourniture = testABit(outData, 10) * 1 + testABit(outData, 11) * 2 + testABit(outData, 12) * 4 + testABit(outData, 13) * 8;
    sensor.addField("ETarifFourniture", ETarifFourniture);
    JSONbuffer["ETarifFourniture"] = String(ETarifFourniture);
    ETarifDistributeur = testABit(outData, 14) * 1 + testABit(outData, 15) * 2;
    sensor.addField("ETarifDistributeur", ETarifDistributeur);
    JSONbuffer["ETarifDistributeur"] = String(ETarifDistributeur);
    EHorloge = testABit(outData, 16) * 1;
    sensor.addField("EHorloge", EHorloge);
    JSONbuffer["EHorloge"] = String(EHorloge);
    ETic = testABit(outData, 17) * 1;
    sensor.addField("ETic", ETic);
    JSONbuffer["ETic"] = String(ETic);
    // Serial.println(testABit(outData, 18) * 1);
    EComEuridis = testABit(outData, 19) * 1 + testABit(outData, 20) * 2;
    sensor.addField("EComEuridis", EComEuridis);
    JSONbuffer["EComEuridis"] = String(EComEuridis);
    EStatusCPL = testABit(outData, 21) * 1 + testABit(outData, 22) * 2;
    sensor.addField("EStatusCPL", EStatusCPL);
    JSONbuffer["EStatusCPL"] = String(EStatusCPL);
    ESyncroCPL = testABit(outData, 23) * 1;
    sensor.addField("ESyncroCPL", ESyncroCPL);
    JSONbuffer["ESyncroCPL"] = String(ESyncroCPL);
    ECouleurJ = testABit(outData, 24) * 1 + testABit(outData, 25) * 2;
    sensor.addField("ECouleurJ", ECouleurJ);
    JSONbuffer["ECouleurJ"] = String(ECouleurJ);
    if (ECouleurJ == 1) {
      sensor.addField("AUJOURDHUI", "BLEU");
      JSONbuffer["AUJOURDHUI"] = String("BLEU");
    } else if (ECouleurJ == 2) {
      sensor.addField("AUJOURDHUI", "BLAN");
      JSONbuffer["AUJOURDHUI"] = String("BLAN");
    } else if (ECouleurJ == 3) {
      sensor.addField("AUJOURDHUI", "ROUG");
      JSONbuffer["AUJOURDHUI"] = String("ROUG");
    } else {
      sensor.addField("AUJOURDHUI", "----");
      JSONbuffer["AUJOURDHUI"] = String("----");
    }
    ECouleurD = testABit(outData, 26) * 1 + testABit(outData, 27) * 2;
    sensor.addField("ECouleurD", ECouleurD);
    JSONbuffer["ECouleurD"] = String(ECouleurD);
    if (ECouleurD == 1) {
      sensor.addField("DEMAIN", "BLEU");
      JSONbuffer["DEMAIN"] = String("BLEU");
    } else if (ECouleurD == 2) {
      sensor.addField("DEMAIN", "BLAN");
      JSONbuffer["DEMAIN"] = String("BLAN");
    } else if (ECouleurD == 3) {
      sensor.addField("DEMAIN", "ROUG");
      JSONbuffer["DEMAIN"] = String("ROUG");
    } else {
      sensor.addField("DEMAIN", "----");
      JSONbuffer["DEMAIN"] = String("----");
    }
    EPrevisPM = testABit(outData, 28) * 1 + testABit(outData, 29) * 2;
    sensor.addField("EPrevisPM", EPrevisPM);
    JSONbuffer["EPrevisPM"] = String(EPrevisPM);
    EPM = testABit(outData, 30) * 1 + testABit(outData, 31) * 2;
    sensor.addField("EPM", EPM);
    JSONbuffer["EPM"] = String(EPM);
  }
  if (okDPM1) {
    sensor.addField("DPM1", String(DPM1));
    JSONbuffer["DPM1"] = String(DPM1);
  }
  if (okFPM1) {
    sensor.addField("FPM1", String(FPM1));
    JSONbuffer["FPM1"] = String(FPM1);
  }
  if (okDPM2) {
    sensor.addField("DPM2", String(DPM2));
    JSONbuffer["DPM2"] = String(DPM2);
  }
  if (okFPM2) {
    sensor.addField("FPM2", String(FPM2));
    JSONbuffer["FPM2"] = String(FPM2);
  }
  if (okDPM3) {
    sensor.addField("DPM3", String(DPM3));
    JSONbuffer["DPM3"] = String(DPM3);
  }
  if (okFPM3) {
    sensor.addField("FPM3", String(FPM3));
    JSONbuffer["FPM3"] = String(FPM3);
  }
  if (okMSG1) {
    sensor.addField("MSG1", String(MSG1));
    JSONbuffer["MSG1"] = String(MSG1);
  }
  if (okMSG2) {
    sensor.addField("MSG2", String(MSG2));
    JSONbuffer["MSG2"] = String(MSG2);
  }
  if (okPRM) {
    sensor.addField("PRM", String(PRM));
    JSONbuffer["PRM"] = String(PRM);
  }
  if (okRELAIS) {
    sensor.addField("RELAIS", String(RELAIS));
    JSONbuffer["RELAIS"] = String(RELAIS);
  }
  if (okNTARF) {
    sensor.addField("NTARF", String(NTARF));
    JSONbuffer["NTARF"] = String(NTARF);
  }
  if (okNJOURF) {
    sensor.addField("NJOURF", String(NJOURF));
    JSONbuffer["NJOURF"] = String(NJOURF);
  }
  if (okNJOURF1) {
    sensor.addField("NJOURF1", String(NJOURF1));
    JSONbuffer["NJOURF1"] = String(NJOURF1);
  }
  if (okPJOURF1) {
    sensor.addField("PJOURF1", String(PJOURF1));
    JSONbuffer["PJOURF1"] = String(PJOURF1);
  }
  if (okPPOINTE) {
    sensor.addField("PPOINTE", String(PPOINTE));
    JSONbuffer["PPOINTE"] = String(PPOINTE);
  }
}

void DataformatComPart2() {
  if (useinflux == 1) {
    // Data point
    sensor.addTag("MacAdress", adressmacesp);
    InfluxDBClient clientinflux(INFLUXDB_URL, INFLUXDB_ORG, INFLUXDB_BUCKET, INFLUXDB_TOKEN);
    clientinflux.setInsecure();
    boolean influxconnec = 0;
    while (!influxconnec) {
      influxconnec = clientinflux.validateConnection();
      delay(100);
    }
    clientinflux.writePoint(sensor);
  }
  if (usemqtt == 1) {
  //  client.setServer(strmqtt_server.c_str(), 1883);    //Configuration de la connexion au serveur MQTT
    if (!client.connected()) {
      reconnect();
    }
    client.loop();
    serializeJson(JSONbuffer, output);
    String maintopic = strmaintopicmqtt + "/" + adressmacesp;
    client.publish(maintopic.c_str(), output.c_str(), true);
    output = "";
  }
}


/* ======================================================================
  Function: loop
  Purpose : infinite loop main code
  Input   : -
  Output  : -
  Comments: -
  ====================================================================== */

void linky_loop() {
  // Teleinformation processing
  if ( Serial.available() ) {
    /*
      char inByte = Serial2.read();
      Serial.print(inByte);
      Serial.print(".");
      Serial.print(inByte,HEX);
      Serial.print(";");
    */
    tinfo.process(Serial.read());
  }

  if (millis() > (MQAll + DeepSleepSecondsOpti)) {
    MQAll = millis();
    testnumber = testnumber + 1;
    DataformatComPart1();
    DataformatComPart2();
  }

  /*
    if ((String(NGTF).indexOf("BA") != -1) && okSINSTS && boucleok == 1 && (millis() > (MQAll + DeepSleepSecondsOpti))) {
      DataformatComPart1();
      DataformatComPart2();
    }
    else if ((String(NGTF).indexOf('H') != -1) && okSINSTS && boucleok == 1&& (millis() > (MQAll + DeepSleepSecondsOpti))) {
      DataformatComPart1();
      sensor.addField("HCHC", atoi(String(EASF01).c_str()));
      sensor.addField("HCHP", atoi(String(EASF02).c_str()));
      DataformatComPart2();
    }
    else if ((String(NGTF).indexOf('T') != -1) && okSINSTS && boucleok == 1&& (millis() > (MQAll + DeepSleepSecondsOpti))) {
      DataformatComPart1();
      sensor.addField("BBRHCJB", atoi(String(EASF01).c_str()));
      sensor.addField("BBRHPJB", atoi(String(EASF02).c_str()));
      sensor.addField("BBRHCJW", atoi(String(EASF03).c_str()));
      sensor.addField("BBRHPJW", atoi(String(EASF04).c_str()));
      sensor.addField("BBRHCJR", atoi(String(EASF05).c_str()));
      sensor.addField("BBRHPJR", atoi(String(EASF06).c_str()));
      DataformatComPart2();
    }
    else if ((String(NGTF).indexOf('E') != -1) && okSINSTS && boucleok == 1&& (millis() > (MQAll + DeepSleepSecondsOpti))) {
      DataformatComPart1();
      sensor.addField("EJPHN", atoi(String(EASF01).c_str()));
      sensor.addField("EJPHPM", atoi(String(EASF02).c_str()));
      DataformatComPart2();
    }
    else if (okNGTF && okSINSTS && boucleok == 1) {
      DataformatComPart1();
      DataformatComPart2();
    }

  */
}
