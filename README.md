Shield: [![CC BY-NC-SA 4.0][cc-by-nc-sa-shield]][cc-by-nc-sa]

This work is licensed under a
[Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License][cc-by-nc-sa].

[![CC BY-NC-SA 4.0][cc-by-nc-sa-image]][cc-by-nc-sa]

[cc-by-nc-sa]: http://creativecommons.org/licenses/by-nc-sa/4.0/
[cc-by-nc-sa-image]: https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png
[cc-by-nc-sa-shield]: https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg

Shield: [![CC BY-NC-ND 4.0][cc-by-nc-nd-shield]][cc-by-nc-nd]

The Hardware is licensed under a
[Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License][cc-by-nc-sa].


[![CC BY-NC-ND 4.0][cc-by-nc-nd-image]][cc-by-nc-nd]

[cc-by-nc-nd]: https://creativecommons.org/licenses/by-nc-nd/4.0/
[cc-by-nc-nd-image]: https://licensebuttons.net/l/by-nc-nd/4.0/88x31.png
[cc-by-nc-nd-shield]: https://img.shields.io/badge/License-CC%20BY--NC--ND%204.0-lightgrey.svg

# EKy

- Eky is an Open-source project which creates a Ethernet end-device to connect with Linky meter in France.

# ESP8266 Pinout

![ESP8266_Dev_board_Pinout](/uploads/d5fe6af51567ca11c54279157e1b1035/ESP8266_Dev_board_Pinout.png)

# W5500 Ethernet module Pinout

![W5500_Pinout](/uploads/4281d55faa7bc19772972048eaa87bfd/W5500_Pinout.png)

# Wireing

|W5500|---|ESP8266|---|Eky|
|:-:|:-:|:-:|:-:|:-:|
|SCK|---|GPIO14| | |
|MISO|---|GPIO12| | |
|MOSI|---|GPIO13| | |
|CS|---|GPIO16| | |
|RST|---|RESET| | |
|GND|---|GND|---|GND|
|3V3|---|3V3|---|3V3|
| | |GPIO3|---|RX|

![Schéma_de_câblage](/uploads/083dbfe04de2075e5835b932932d6b62/Schéma_de_câblage.png)
![Photo_de_câblage](/uploads/9e663557a353d93974819c978a88daa5/Photo_de_câblage.png)

# Original code

We use the code of [@nopnop2002][1] as base

[1] : https://github.com/nopnop2002/esp8266_ethernet

![CNRS-logo][]
![G2elab-logo][]
![UGA-logo][]
![GINP-logo][]


[GINP-logo]: https://www.grenoble-inp.fr/uas/alias1/LOGO/Grenoble-INP-Etablissement-120px.png
[CNRS-logo]: https://upload.wikimedia.org/wikipedia/fr/thumb/7/72/Logo_Centre_national_de_la_recherche_scientifique_%282023-%29.svg/langfr-130px-Logo_Centre_national_de_la_recherche_scientifique_%282023-%29.svg.png
[G2elab-logo]: https://g2elab.grenoble-inp.fr/uas/alias31/LOGO/G2Elab+logo.png
[UGA-logo]:https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR1D0TxrJin49KlU9Oo263OgjsanU9vKQZhKJ9LZsMjxxggnCEKO7NDsN-rW6CEXGJbLEE&usqp=CAU

